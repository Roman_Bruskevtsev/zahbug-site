<?php
/**
 *
 * @package WordPress
 * @subpackage Zahbug
 * @since 1.0
 * @version 1.0
 */
$zahbug = new ZahbugClass();
$header_class = is_page_template('srm-page.php') ? ' class="static"' : '';
$mobile_class = is_page_template('srm-page.php') ? ' static' : '';
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178661703-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-178661703-1');
    </script>
    <?php wp_head(); ?>
</head>
<body <?php body_class('load'); ?>>
    <?php wp_body_open(); ?>
    <div class="circle top"></div>
    <div class="circle left"></div>
    <div class="circle bottom"></div>
    <header<?php echo $header_class; ?>>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <?php 
                    $logo = get_field('logo', 'option');
                    if( $logo ) { ?>
                    <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } 
                    if( has_nav_menu('main') ) { ?>
                    <div class="menu__block float-left">
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'main__nav'
                        ) ); ?>
                    </div>
                    <?php } ?>
                    <div class="right__nav float-right">
                        <?php 
                        if( has_nav_menu('right-menu') ) { ?>
                        <div class="cabinet__block float-left">
                            <?php wp_nav_menu( array(
                                'theme_location'        => 'right-menu',
                                'container'             => 'nav',
                                'container_class'       => 'cabinet__nav'
                            ) ); ?>
                        </div>
                        <?php }
                        echo $zahbug->language_switcher(); ?>
                    </div>
                    <div class="mobile__btn float-right">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="menu__mobile<?php echo $mobile_class; ?>">
        <div class="right__nav">
            <?php 
            if( has_nav_menu('right-menu') ) { ?>
            <div class="cabinet__block float-left">
                <?php wp_nav_menu( array(
                    'theme_location'        => 'right-menu',
                    'container'             => 'nav',
                    'container_class'       => 'cabinet__nav'
                ) ); ?>
            </div>
            <?php }
            echo $zahbug->language_switcher(); ?>
        </div>
        <?php if( has_nav_menu('main') ) { ?>
        <div class="menu__block float-left">
            <?php wp_nav_menu( array(
                'theme_location'        => 'main',
                'container'             => 'nav',
                'container_class'       => 'main__nav'
            ) ); ?>
        </div>
        <?php } ?>
    </div>
    <main>