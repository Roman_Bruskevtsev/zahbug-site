<?php
/**
 *
 * @package WordPress
 * @subpackage Zahbug
 * @since 1.0
 * @version 1.0
 */

get_header(); 

get_template_part( 'template-parts/page/content', 'title' );

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'main_banner' ): 
            get_template_part( 'inc/acf-content/main_banner' );
        elseif( get_row_layout() == 'service_section' ): 
            get_template_part( 'inc/acf-content/service_section' );
        elseif( get_row_layout() == 'service_section_normal' ): 
            get_template_part( 'inc/acf-content/service_section_normal' );
        elseif( get_row_layout() == 'news_section' ): 
            get_template_part( 'inc/acf-content/news_section' );
        elseif( get_row_layout() == 'content_section' ): 
            get_template_part( 'inc/acf-content/content_section' );
        elseif( get_row_layout() == 'press_section' ): 
            get_template_part( 'inc/acf-content/press_section' );
        elseif( get_row_layout() == 'contacts_section' ): 
            get_template_part( 'inc/acf-content/contacts_section' );
        elseif( get_row_layout() == 'vacancies_section' ): 
            get_template_part( 'inc/acf-content/vacancies_section' );
        elseif( get_row_layout() == 'tabs_section' ): 
            get_template_part( 'inc/acf-content/tabs_section' );
        elseif( get_row_layout() == 'map_section' ): 
            get_template_part( 'inc/acf-content/map_section' );
        elseif( get_row_layout() == 'form_section' ): 
            get_template_part( 'inc/acf-content/form_section' );
        elseif( get_row_layout() == 'two_column_text_section' ): 
            get_template_part( 'inc/acf-content/two_column_text_section' );
        elseif( get_row_layout() == 'products_and_services_section' ): 
            get_template_part( 'inc/acf-content/products_and_services_section' );
        elseif( get_row_layout() == 'faq_section' ): 
            get_template_part( 'inc/acf-content/faq_section' );
        elseif( get_row_layout() == 'spare_parts_section' ): 
            get_template_part( 'inc/acf-content/spare_parts_section' );
        elseif( get_row_layout() == 'equpments_section' ): 
            get_template_part( 'inc/acf-content/equpments_section' );
        elseif( get_row_layout() == 'images_navigations_block' ): 
            get_template_part( 'inc/acf-content/images_navigations_block' );
        elseif( get_row_layout() == 'announcements_block' ): 
            get_template_part( 'inc/acf-content/announcements_block' );
        elseif( get_row_layout() == 'contacts_of_departments' ): 
            get_template_part( 'inc/acf-content/contacts_of_departments' );
        endif;
    endwhile;
endif;

get_footer();