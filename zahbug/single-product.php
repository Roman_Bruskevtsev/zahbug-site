<?php
/**
 *
 * @package WordPress
 * @subpackage Zahbug
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) { the_post();
		$background = ( get_field('image') ) ? ' style="background-image: url('.get_field('image')['url'].');"' : ''; 
		get_template_part( 'template-parts/service/content', 'title' ); ?>
		<section class="product__section">
			<div class="container">
				<div class="row justify-content-end">
					<div class="col-md-9 col-lg-9">
						<div class="product__thumbnail" data-aos="fade-right" data-aos-duration="600"<?php echo $background; ?>></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-lg-6">
						<div class="product__info" data-aos="fade-up" data-aos-duration="600">
							<?php if( get_field('title') ) { ?><h5><b><?php the_field('title'); ?></b></h5><?php } 
							$products = get_field('products');
							if( $products ){ ?>
								<ul class="products__list">
									<?php foreach ( $products as $product ) { 
										if( $product['image'] ) { ?>
										<li>
											<a class="product" href="<?php echo $product['image']['url']; ?>"><?php echo $product['name']; ?></a>
										</li>
										<?php }
									} ?>
								</ul>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php }
}

get_footer();