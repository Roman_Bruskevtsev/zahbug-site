<?php
/**
 *
 * @package WordPress
 * @subpackage Zahbug
 * @since 1.0
 * @version 1.0
 */
$footer_class = is_page_template('srm-page.php') ? ' class="nomargin"' : '';
?>  
    </main>
    <footer<?php echo $footer_class; ?>>
        <?php 
        $logo = get_field('footer_logo', 'option');
        if( $logo ) { ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                </div>
            </div>
        </div>
        <?php } ?>
    	<div class="container">
    		<div class="row">
    			<?php if ( is_active_sidebar( 'footer-1' ) ) { ?><div class="col-lg-3"><?php dynamic_sidebar( 'footer-1' ); ?></div><?php } ?>
    			<?php if ( is_active_sidebar( 'footer-2' ) ) { ?><div class="col-lg-5"><?php dynamic_sidebar( 'footer-2' ); ?></div><?php } ?>
    			<?php if ( is_active_sidebar( 'footer-3' ) ) { ?><div class="col-lg-4"><?php dynamic_sidebar( 'footer-3' ); ?></div><?php } ?>
    		</div>
    	</div>
        <div class="bottom__line">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <?php 
                        $copyrights = get_field('copyrights', 'option');
                        if( $copyrights ) { ?>
                            <div class="copyrights float-left"><?php echo $copyrights; ?></div>
                        <?php } 
                        $social_label = get_field('social_label', 'option');
                        $facebook_link = get_field('facebook_link', 'option'); 
                        if( $facebook_link ){ ?>
                            <div class="social__block float-right">
                                <?php if( $social_label ){ ?><p><?php echo $social_label; ?></p><?php } ?>
                                <a href="<?php echo $facebook_link; ?>" target="_blank" class="facebook"></a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="fixed__block">
        <?php 
        $service_page = get_field('service_page', 'option');
        $form_shortcode = get_field('contact_form', 'option')['form_shortcode'];
        if( (int) $service_page == get_the_ID() || is_singular( array( 'service', 'product' ) ) ) { ?>
            <div class="form__popup"><?php _e('Have a question?', 'zahbug'); ?></div>
        <?php } ?>
        <div class="to__top"></div>
    </div>
    <div class="popup__wrapper"></div>
    <?php 
    if( (int) $service_page == get_the_ID() || is_singular( array( 'service', 'product' ) ) ) { ?>
    <div class="form__popup__block">
        <div class="close"></div>
        <?php echo do_shortcode($form_shortcode); ?>
    </div>
    <?php }
    wp_footer(); ?>
</body>
</html>