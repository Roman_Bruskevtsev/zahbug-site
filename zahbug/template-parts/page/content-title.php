<?php 
$image_1 = get_field('image_1');
$image_2 = get_field('image_2');

$class = $image_1 || $image_2 ? ' has__image' : ' no__image';
?>
<section class="page__title<?php echo $class; ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="content" data-aos="fade-right" data-aos-duration="600">
					<h1 class="h2"><b><?php the_title(); ?></b></h1>
					<?php the_field('subtitle'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if( $image_1 || $image_2 ) { ?>
<section class="title__images">
	<div class="container-fluid nopadding">
		<div class="row justify-content-end">
			<div class="col-lg-6">
				<div class="images">
				<?php if( $image_1 ) { ?>
					<div class="image image__1" data-aos="fade-left" data-aos-duration="600">
						<img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['title']; ?>">
					</div>
				<?php } ?>
				<?php if( $image_2 ) { ?>
					<div class="image image__2" data-aos="fade-up" data-aos-duration="600">
						<img src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['title']; ?>">
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>