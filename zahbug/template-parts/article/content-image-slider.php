<?php $images = get_sub_field('images'); 
if( $images ) { ?>
<section class="images__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="image__slider" data-aos="fade-up">
					<?php foreach ( $images as $image ) { ?>
					<div class="slide">
						<div class="image">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>