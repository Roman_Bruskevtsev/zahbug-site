<section class="post__text">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="text" data-aos="fade-up"><?php the_sub_field('text_1'); ?></div>
			</div>
			<div class="col-md-6">
				<div class="text" data-aos="fade-up"><?php the_sub_field('text_2'); ?></div>
			</div>
		</div>
	</div>
</section>