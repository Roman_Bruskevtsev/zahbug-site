<?php 
$format = get_post_format(get_the_ID());
?>
<a class="press__block <?php echo $format; ?>" href="<?php the_permalink(); ?>" data-aos="fade-up" data-aos-duration="1000">
	<div class="date">
		<?php echo get_the_date(); ?>
	</div>
	<h6><?php the_title(); ?></h6>
	<?php the_excerpt(); ?>
</a>