<?php
$article_id = get_field('article_page', 'option'); 
?>
<section class="post__title">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php if( $article_id ) { ?>
				<div class="blog__nav" data-aos="fade-up">
					<a href="<?php echo get_the_permalink( $article_id ); ?>"><?php _e('Go back', 'zahbug'); ?></a>
				</div>
				<?php } ?>
				<div class="title" data-aos="fade-up">
					<h1 class="h3"><b><?php the_title(); ?></b></h1>
				</div>
			</div>
		</div>
	</div>
</section>