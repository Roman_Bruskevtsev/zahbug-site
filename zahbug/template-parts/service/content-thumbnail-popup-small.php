<?php $background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'service-thumbnails' ).');"' : ''; ?>
<div<?php echo $background; ?> class="service__thumbnail show__popup small" data-aos="fade-up" data-id="<?php echo get_the_ID(); ?>">
	<h6><span><?php the_title(); ?></span></h6>
</div>