<?php
$parent_id = get_post_parent(get_the_ID());
$page_id = get_post_parent(get_the_ID()) ? $parent_id->ID : get_field('service_page', 'option'); 
?>
<section class="post__title">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<?php if( $page_id ) { ?>
				<div class="blog__nav" data-aos="fade-up">
					<a href="<?php echo get_the_permalink( $page_id ); ?>"><?php _e('Go back', 'zahbug'); ?></a>
				</div>
				<?php } 
				if( !get_field('hide_title') ) { ?>
				<div class="title" data-aos="fade-up">
					<h1 class="h3"><b><?php the_title(); ?></b></h1>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>