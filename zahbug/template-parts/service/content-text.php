<section class="post__text">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="text" data-aos="fade-up"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
	</div>
</section>