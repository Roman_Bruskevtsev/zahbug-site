<?php 
$blocks_type = get_sub_field('blocks_type');
$blocks_size = $blocks_type == '0' ? 'col-lg-6 col-xl-4' : 'col-lg-6';
$blocks = get_sub_field('blocks'); 
$class = $blocks_type == '0' ? '' : ' small';
?>
<section class="informations__blocks__section">
	<div class="container">
		<?php 
		if( get_sub_field('title') ){ ?>
		<div class="row">
			<div class="col-md-8">
				<div class="section__title" data-aos="fade-up">
					<h5><b><?php the_sub_field('title'); ?></b></h5>
				</div>
			</div>
		</div>
		<?php }
		if( $blocks ) { ?>
		<div class="row">
			<?php foreach ( $blocks as $block ) { ?>
				<div class="<?php echo $blocks_size; ?>">
					<div class="informations__block<?php echo $class; ?>" data-aos="fade-up">
						<?php if( $block['image'] ) { ?>
							<div class="image" style="background-image: url(<?php echo $block['image']['url']; ?>);"></div>
						<?php } 
						if( $block['text'] ) { ?>
							<div class="text"><?php echo $block['text']; ?></div>
						<?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>