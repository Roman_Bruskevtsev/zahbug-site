<?php $background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'service-thumbnails' ).');"' : ''; ?>
<a<?php echo $background; ?> href="<?php the_permalink(); ?>" class="service__thumbnail small" data-aos="fade-up">
	<h6><span><?php the_title(); ?></span></h6>
</a>