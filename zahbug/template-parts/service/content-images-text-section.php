<section class="images__text__section">
	<div class="container">
		<div class="row">
			<?php if( get_sub_field('images') ) { ?>
			<div class="col-md-6">
				<div class="images__block">
					<?php foreach ( get_sub_field('images') as $image) { ?>
						<div class="image" data-aos="fade-up">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
						</div>
					<?php } ?>
				</div>
			</div>
			<?php } 
			if( get_sub_field('text') ) {?>
			<div class="col-md-6">
				<div class="text__block" data-aos="fade-left"><?php the_sub_field('text'); ?></div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>