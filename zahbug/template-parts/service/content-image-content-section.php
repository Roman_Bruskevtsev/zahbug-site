<?php 
$image = get_sub_field('image');
$content =  get_sub_field('content'); ?>
<section class="image__content__section">
	<div class="container">
		<div class="row">
			<?php if( $image ) { ?>
			<div class="col-lg-6">
				<div class="image" data-aos="fade-up">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
				</div>
			</div>	
			<?php }
			if( $content ) { ?>
			<div class="col-lg-6">
				<?php foreach ( $content as $block) {
					if( $block['acf_fc_layout'] == 'text_block') { ?>
						<div class="text__block" data-aos="fade-up"><?php echo $block['text']; ?></div>
					<?php } elseif ( $block['acf_fc_layout'] == 'three_blocks' ) { ?>
						<div class="three__blocks" data-aos="fade-up">
							<?php if( $block['title'] ) { ?>
							<div class="row">
								<div class="col">
									<h6><b><?php echo $block['title']; ?></b></h6>
								</div>
							</div>
							<?php } 
							if( $block['blocks'] ) { ?>
							<div class="row">
								<?php foreach ( $block['blocks'] as $block ) { ?>
								<div class="col-md-4">
									<div class="three__block">
										<?php if( $block['title'] ) { ?><p><?php echo $block['title']; ?></p><?php } ?>
										<?php if( $block['value'] ) { ?><h5><b><?php echo $block['value']; ?></b></h5><?php } ?>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php } ?>
						</div>
					<?php } elseif ( $block['acf_fc_layout'] == 'two_blocks_row' ) { ?>
						<div class="row">
							<?php foreach ( $block['two_blocks'] as $block ) { ?>
								<div class="col-md-6">
									<div class="two_blocks_block" data-aos="fade-up">
										<h6><span><?php echo $block['text']; ?></span></h6>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php }
				}; ?>
			</div>
			<?php } ?>
		</div>
	</div>
</section>