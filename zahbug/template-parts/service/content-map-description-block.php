<section class="map__description__block">
	<div class="container">
		<div class="row justify-content-center">
			<?php if( get_sub_field('map') ) { ?>
			<div class="col-md-6">
				<div class="map" data-aos="fade-right">
					<img src="<?php echo get_sub_field('map')['url']; ?>" alt="<?php echo get_sub_field('map')['title']; ?>">
				</div>
			</div>
			<?php } 
			$descriptions = get_sub_field('descriptions'); 
			if( $descriptions ) { ?>
			<div class="col-md-4">
				<div class="descriptions__list" data-aos="fade-left">
					<ul>
					<?php foreach ( $descriptions as $description ) { ?>
						<li>
							<span style="background-color: <?php echo $description['color']; ?>"></span>
						<?php if( $description['title'] ) { ?>
							<h6><?php echo $description['title']; ?></h6>
						<?php } 
						if( $description['description'] ) { ?>
							<p><?php echo $description['description']; ?></p>
						<?php } ?>
						</li>
					<?php } ?>
					</ul>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>