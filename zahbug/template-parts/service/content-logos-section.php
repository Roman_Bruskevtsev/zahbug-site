<section class="logos__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="section__title" data-aos="fade-up">
					<h5><b><?php the_sub_field('title'); ?></b></h5>
				</div>
			</div>
		</div>
		<?php } 
		$logos_grid = 'row-cols-1 row-cols-md-2 row-cols-lg-4';
		$logos_grid_type = get_sub_field('logos_grid');
		switch ($logos_grid_type) {
			case '0':
				$logos_grid = 'row-cols-1 row-cols-md-2 row-cols-lg-4';
				break;
			
			default:
				$logos_grid = 'row-cols-1 row-cols-md-2 row-cols-lg-5';
				break;
		}
		$logos = get_sub_field('logos'); 
		if( $logos ) { ?>
		<div class="row justify-content-center <?php echo $logos_grid; ?>">
			<?php foreach ( $logos as $logo ) { ?>
			<div class="col">
				<div class="logo__block" data-aos="fade-up">
					<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['title']; ?>">
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>