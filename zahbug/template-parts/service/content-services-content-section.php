<?php 
$services = get_sub_field('services');
$show_popup = get_sub_field('show_services_in_popup');
$content =  get_sub_field('content'); ?>
<section class="services__content__section">
	<div class="container">
		<div class="row">
			<?php if( $services ) { 
				$args = array(
					'posts_per_page' 	=> -1,
					'post_type' 		=> 'service',
					'post__in'			=> $services,
					'orderby'			=> 'post__in'
				); 
				$query = new WP_Query( $args );	
				if ( $query->have_posts() ) { ?>
					<div class="col-lg-6">
						<div class="row">
						<?php while ( $query->have_posts() ) { $query->the_post(); ?>
							<div class="col-md-6">
								<?php
								if( $show_popup ){
									get_template_part( 'template-parts/service/content', 'thumbnail-popup-small' ); 
								} else {
									get_template_part( 'template-parts/service/content', 'thumbnail-link-small' ); 
								} ?>
							</div>
						<?php } ?>
						</div>
					</div>
				<?php } wp_reset_postdata(); 
			} 
			if( $content ) { ?>
			<div class="col-lg-6">
				<?php foreach ( $content as $block) {
					if( $block['acf_fc_layout'] == 'text_block') { ?>
						<div class="text__block" data-aos="fade-up"><?php echo $block['text']; ?></div>
					<?php } elseif ( $block['acf_fc_layout'] == 'three_blocks' ) { ?>
						<div class="three__blocks" data-aos="fade-up">
							<?php if( $block['title'] ) { ?>
							<div class="row">
								<div class="col">
									<h6><b><?php echo $block['title']; ?></b></h6>
								</div>
							</div>
							<?php } 
							if( $block['blocks'] ) { ?>
							<div class="row">
								<?php foreach ( $block['blocks'] as $block ) { ?>
								<div class="col-md-4">
									<div class="three__block">
										<?php if( $block['title'] ) { ?><p><?php echo $block['title']; ?></p><?php } ?>
										<?php if( $block['value'] ) { ?><h5><b><?php echo $block['value']; ?></b></h5><?php } ?>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php } ?>
						</div>
					<?php } elseif ( $block['acf_fc_layout'] == 'two_blocks_row' ) { ?>
						<div class="row">
							<?php foreach ( $block['two_blocks'] as $block ) { ?>
								<div class="col-md-6">
									<div class="two_blocks_block" data-aos="fade-up">
										<h6><span><?php echo $block['text']; ?></span></h6>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php }
				}; ?>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php if( $show_popup ) { ?>
	<div class="popup__block service__popup">
		<span class="close"></span>
		<div class="body"></div>
	</div>
	<?php } ?>
</section>