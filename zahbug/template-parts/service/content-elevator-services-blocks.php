<section class="elevator__services__blocks">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="section__title" data-aos="fade-up">
					<h5><b><?php the_sub_field('title'); ?></b></h5>
				</div>
			</div>
		</div>
		<?php } 
		$blocks = get_sub_field('blocks');
		if( $blocks ) { ?>
		<div class="row">
			<?php foreach ( $blocks as $block ) { ?>
			<div class="col-md-3">
				<div class="elevator__service" data-aos="fade-up">
					<?php if( $block['image'] ) { ?>
						<div class="image" style="background-image: url(<?php echo $block['image']['url']; ?>);"></div>
					<?php }
					if( $block['text'] ) { ?>
						<div class="text"><p><?php echo $block['text']; ?></p></div>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>