<?php 
$width_left = $width_right = 'col-md-6';
switch ( get_sub_field('columns_width') ) {
	case '0':
		$width_left = $width_right = 'col-md-6';
		break;
	case '1':
		$width_left = 'col-md-7';
		$width_right = 'col-md-5';
		break;
	default:
		# code...
		break;
}
?>
<section class="post__text">
	<div class="container">
		<div class="row">
			<div class="<?php echo $width_left; ?>">
				<div class="text" data-aos="fade-up"><?php the_sub_field('text_1'); ?></div>
			</div>
			<div class="<?php echo $width_right; ?>">
				<div class="text" data-aos="fade-up"><?php the_sub_field('text_2'); ?></div>
			</div>
		</div>
	</div>
</section>