<?php
$blocks = get_sub_field('blocks');
if( $blocks ) { ?>
<section class="half__text__image__information__block">
	<div class="container">
		<div class="row">
		<?php foreach ($blocks as $block) { ?>
			<div class="col-md-6">
				<div class="text__info__block" data-aos="fade-up">
				<?php if( $block['text'] ) { ?>
					<div class="row">
						<div class="col">
							<div class="text"><?php echo $block['text']; ?></div>
						</div>
					</div>
				<?php } 
				if( $block['image'] || $block['information_block']  ) { ?>
					<div class="row">
						<?php if( $block['image'] ) { ?>
						<div class="col-md-6">
							<div class="image">
								<img src="<?php echo $block['image']['url']; ?>" alt="<?php echo $block['image']['title']; ?>">
							</div>
						</div>
						<?php } 
						if( $block['information_block'] ) { ?>
						<div class="col-md-6">
							<div class="information__block">
								<?php if( $block['information_block']['title'] ) { ?>
									<h6><?php echo $block['information_block']['title']; ?></h6>
								<?php } 
								if( $block['information_block']['value'] ) { ?>
									<h5><?php echo $block['information_block']['value']; ?></h5>
								<?php } 
								if( $block['information_block']['description'] ) { ?>
									<p><?php echo $block['information_block']['description']; ?></p>
								<?php } ?>
							</div>
						</div>
						<?php } ?>
					</div>
				<?php } ?>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
</section>
<?php } ?>