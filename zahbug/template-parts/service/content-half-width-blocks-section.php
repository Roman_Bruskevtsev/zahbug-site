<section class="half__width__blocks__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="section__title" data-aos="fade-up">
					<h5><b><?php the_sub_field('title'); ?></b></h5>
				</div>
			</div>
		</div>
		<?php } 
		$blocks = get_sub_field('blocks'); 
		if( $blocks ) { ?>
		<div class="row">
			<?php foreach ( $blocks as $block ) { ?>
			<div class="col-md-6">
				<div class="icons__block" data-aos="fade-up">
					<?php if( $block['icon'] ) { ?>
					<div class="icon">
						<img src="<?php echo $block['icon']['url']; ?>" alt="<?php echo $block['icon']['title']; ?>">
					</div>
					<?php } 
					if( $block['title'] ) { ?><h6><b><?php echo $block['title']; ?></b></h6><?php } 
					echo $block['text']; ?>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>