<section class="products__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="section__title" data-aos="fade-up">
					<h3><b><?php the_sub_field('title'); ?></b></h3>
				</div>
			</div>
		</div>
		<?php } 
		$products = get_sub_field('products'); 
		if( $products ) { 
			$args = array(
				'posts_per_page' 	=> -1,
				'post_type' 		=> 'product',
				'post__in'			=> $products,
				'orderby'			=> 'post__in'
			); 
			$query = new WP_Query( $args );	
			if ( $query->have_posts() ) { ?> 
			<div class="row row-cols-1 row-cols-md-3 row-cols-lg-5">
				<?php while ( $query->have_posts() ) { $query->the_post(); ?>
					<div class="col">
						<?php get_template_part( 'template-parts/product/content', 'thumbnail-link' ); ?>
					</div>
				<?php } ?>
			</div>
			<?php } wp_reset_postdata(); 
		} ?>
	</div>
</section>