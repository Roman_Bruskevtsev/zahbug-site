<section class="text__image__blocks__section">
	<div class="container">
		<div class="row">
			<?php
			if( get_sub_field('image') ) { ?>
			<div class="col-lg-6">
				<div class="image" data-aos="fade-up">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
			</div>
			<?php } 
			$text = get_sub_field('text');
			$blocks = get_sub_field('blocks'); 
			if( $blocks || $text ) { ?>
			<div class="col-lg-6">
				<?php if( $text ) { ?>
					<div class="row">
						<div class="col">
							<div class="text" data-aos="fade-up"><?php the_sub_field('text'); ?></div>
						</div>
					</div>
				<?php } 
				if( $blocks ) { ?>
				<div class="row">
					<?php foreach ( $blocks as $block ) { ?>
					<div class="col-md-6">
						<div class="text__block" data-aos="fade-up">
							<?php if( $block['text'] ) { ?><h6><?php echo $block['text']; ?></h6><?php } ?>
							<?php if( $block['value'] ) { ?><h5><?php echo $block['value']; ?></h5><?php } ?>
						</div>
					</div>
					<?php } ?>
				</div>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
	</div>
</section>