<section class="sharing">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="sharing__block" data-aos="fade-up">
					<h6><?php _e('Share', 'zahbug'); ?></h6>
					<a href="#" class="facebook"></a>
				</div>
				<div class="post__navigation" data-aos="fade-up">
					<?php
					previous_post_link('%link', '<span></span>'.__('Previous post', 'zahbug'));
					next_post_link('%link', __('Next post', 'zahbug').'<span></span>');
					?>
				</div>
			</div>
		</div>
	</div>
</section>