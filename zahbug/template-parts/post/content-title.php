<?php
$blog_id = get_option('page_for_posts'); 
?>
<section class="post__title">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php if( $blog_id ) { ?>
				<div class="blog__nav" data-aos="fade-up">
					<a href="<?php echo get_the_permalink( $blog_id ); ?>"><?php _e('To all news', 'zahbug'); ?></a>
				</div>
				<?php } ?>
				<div class="post__date" data-aos="fade-up">
					<span><?php echo get_the_date(); ?></span>
				</div>
				<div class="title" data-aos="fade-up">
					<h1 class="h3"><b><?php the_title(); ?></b></h1>
				</div>
			</div>
		</div>
	</div>
</section>