<?php $videos = get_sub_field('videos'); 
if( $videos ) { ?>
<section class="videos__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="video__slider" data-aos="fade-up">
				<?php foreach ( $videos as $video ) { ?>
					<div class="slide">
						<div class="video"><?php echo $video['video']; ?></div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>