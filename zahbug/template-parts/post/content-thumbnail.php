<?php $background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'new-thumbnails' ).');"' : ''; ?>
<a href="<?php the_permalink(); ?>" class="article__block">
	<div class="thumbnail" data-aos="fade-up"<?php echo $background; ?> data-aos-duration="1000"></div>
	<div class="excerpt" data-aos="fade-left" data-aos-duration="1000">
		<div class="date">
			<?php echo get_the_date(); ?>
		</div>
		<h6><?php the_title(); ?></h6>
		<?php the_excerpt(); ?>
	</div>
</a>