<?php 
$terms = get_the_terms(get_the_ID(), 'vacancy-location'); 
$locations_name = $locations_ids = '';
if( $terms ) {
	foreach ($terms as $location) {
		if( $locations_name) {
			$locations_name .= ', ' + $location->name;
		} else {
			$locations_name = $location->name;
		}

		if( $locations_ids ){
			$locations_ids .= ',' + $location->term_id;
		} else {
			$locations_ids = $location->term_id;
		}
	}
} ?>
<div class="col-lg-6 vacancy__row" data-id="<?php echo $locations_ids; ?>">
	<div class="vacancy__block" data-aos="fade-up" data-aos-duration="600">
		<div class="show" onclick="zahbug.vacancyShow(this);"></div>
		<h6><?php the_title(); ?></h6>
		<div class="details__row">
			<div class="location"><?php echo $locations_name; ?></div>
			<div class="date"><?php the_field('date'); ?></div>
			<div class="salary"><?php the_field('salary'); ?> грн</div>
		</div>
		<div class="description"><?php the_content(); ?></div>
	</div>
</div>