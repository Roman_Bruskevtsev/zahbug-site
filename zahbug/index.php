<?php
/**
 *
 * @package WordPress
 * @subpackage Zahbug
 * @since 1.0
 * @version 1.0
 */
get_header();

$blog_id = get_option('page_for_posts'); 
if( $blog_id ) { ?>
<section class="blog__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="title"><h2><b><?php echo get_the_title($blog_id); ?></b></h2></div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="categories__nav">
					<span class="value" data-placeholder="<?php _e('Choose category', 'zahbug'); ?>"><?php _e('All Caregories', 'zahbug'); ?></span>
					<ul>
						<li class="active">
							<a href="<?php echo get_permalink($blog_id); ?>"><?php _e('All Caregories', 'zahbug'); ?></a>
						</li>
						<?php 
						$args = array(
							'taxonomy'		=> 'category',
							'hide_empty'	=> false
						);
						$categories = get_terms($args);
						foreach ( $categories as $category ) { ?>
							<li><a href="<?php echo get_category_link($category->term_id); ?>"><?php echo $category->name; ?></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="posts__row row">
					<?php 
					$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

					$args = array(
						'post_type'			=> 'post',
						'posts_per_page' 	=> $posts_per_page,
						'post_status'		=> 'publish',
						'paged'				=> $paged
					);

					$query = new WP_Query( $args );

					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) { $query->the_post(); ?>
							<div class="col-lg-12">
								<?php get_template_part( 'template-parts/post/content', 'thumbnail' ); ?>
							</div>
						<?php } ?>
					<?php }
					wp_reset_postdata(); ?>
				</div>
				<div class="pagination__row row">
					<div class="col text-center">
						<?php if( $query->max_num_pages > 1 && $query->max_num_pages != $paged ) { ?>
						<button class="btn" onclick="zahbug.loadPosts(<?php echo $query->max_num_pages; ?>, <?php echo $paged; ?>, '', this)">
							<span class="left"></span>
							<?php _e('Show more', 'bca'); ?>
							<span class="right"></span>
						</button>
						<?php } ?>
					</div>
			    </div>
			</div>
		</div>
	</div>
</section>
<?php } get_footer();