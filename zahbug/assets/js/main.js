'use strict';

(function($) {
	$(window).scroll(function(){
    	var scrollHeight = $(document).scrollTop(),
    		windowHeight = window.innerHeight;
    	
    	if (scrollHeight >= 100){
    		$('header').addClass('scroll');
    	} else {
    		$('header').removeClass('scroll');
    	}
    });
    
    $(window).on('load', function(){
        /*Mobile menu*/
        $('.mobile__btn').on('click', function(){
            $(this).toggleClass('show');
            $('.menu__mobile').toggleClass('show');
        });
        
        var scrollHeight = $(document).scrollTop(),
            windowHeight = window.innerHeight;
        
        if (scrollHeight >= 100){
            $('header').addClass('scroll');
        } else {
            $('header').removeClass('scroll');
        }

        $('.products__list').lightGallery({
            selector: '.product',
            thumbnail: false
        });    

        $('.to__top').on('click', function(){
            $('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });

        $('.show__form').on('click', function(){
            $('.vacancy__form').addClass('show');
            $(this).addClass('hide');
        });

        $('.submit__group .cancel').on('click', function(){
            $('.vacancy__form').removeClass('show');
            $('.show__form').removeClass('hide');
        });
        
        if($('.video__slider').length){
            $('.video__slider').slick({
                infinite:       false,
                autoplay:       false,
                speed:          700,
                arrows:         true,
                dots:           false,
                fade:           false
            });
        }

        if($('.image__slider').length){
            $('.image__slider').slick({
                infinite:       false,
                autoplay:       false,
                slidesToShow:   3,
                speed:          700,
                arrows:         true,
                dots:           false,
                fade:           false,
                responsive:     [
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 1
                      }
                    }
                ]
            });
        }

        let contactBlocks = document.querySelectorAll('.contacts__block')

        if( contactBlocks ){
            contactBlocks.forEach( (block) => {
                let moreBtn = block.querySelector('.show__more');
                moreBtn.addEventListener('click', () => {
                    if( block.classList.contains('show') ){
                        block.classList.remove('show');
                    } else {
                        block.classList.add('show');
                    }
                });
            });
        }

        /*Google map*/
        $(function() {
            var marker = [],
                infowindow = [],
                urlParams = new URLSearchParams(location.search),
                map, image = $('.map__block').attr('data-marker');

            function addMarker(location, index, name, contentstr) {
                marker[index] = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: image
                });
                marker[index].setMap(map);

                infowindow[index] = new google.maps.InfoWindow({
                    content: contentstr
                });
                
                google.maps.event.addListener(marker[index], 'click', function() {
                    infowindow[index].open(map, marker[index]);
                });
            }

            function initialize() {
                var lat = $('.map__block').attr("data-lat");
                var lng = $('.map__block').attr("data-lng");
                var myLatlng = new google.maps.LatLng(lat, lng);

                var setZoom = parseInt($('.map__block').attr("data-zoom"));

                var styles = [
                                {
                                    "featureType": "administrative",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative",
                                    "elementType": "labels.text",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        },
                                        {
                                            "color": "#8e8e8e"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#7f7f7f"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative",
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative.country",
                                    "elementType": "geometry.stroke",
                                    "stylers": [
                                        {
                                            "color": "#bebebe"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative.province",
                                    "elementType": "geometry.stroke",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        },
                                        {
                                            "color": "#cbcbcb"
                                        },
                                        {
                                            "weight": "0.69"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "administrative.locality",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        },
                                        {
                                            "saturation": "0"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.attraction",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        },
                                        {
                                            "color": "#ffffff"
                                        },
                                        {
                                            "saturation": "0"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.attraction",
                                    "elementType": "labels",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.business",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.government",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.medical",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.park",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#98da9f"
                                        },
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.park",
                                    "elementType": "labels",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.place_of_worship",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.school",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.sports_complex",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.sports_complex",
                                    "elementType": "labels",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi.sports_complex",
                                    "elementType": "labels.text",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "saturation": "-100"
                                        },
                                        {
                                            "lightness": "50"
                                        },
                                        {
                                            "gamma": "1"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#e4e4e4"
                                        },
                                        {
                                            "saturation": "0"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        },
                                        {
                                            "saturation": "0"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#f2f2f2"
                                        },
                                        {
                                            "saturation": "0"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "labels",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "labels.text",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "saturation": "0"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "labels.text",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "labels.icon",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.local",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.local",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "color": "#e4e4e4"
                                        },
                                        {
                                            "lightness": "0"
                                        },
                                        {
                                            "gamma": "1"
                                        },
                                        {
                                            "saturation": "0"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.local",
                                    "elementType": "labels.text",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "transit",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "transit",
                                    "elementType": "labels",
                                    "stylers": [
                                        {
                                            "hue": "#ff0000"
                                        },
                                        {
                                            "saturation": "-100"
                                        },
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "color": "#cbcbcb"
                                        },
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#f3f3f3"
                                        },
                                        {
                                            "saturation": "0"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "labels",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "labels.text",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                }
                            ];

                var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

                var mapOptions = {
                    zoom: setZoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng
                };
                map = new google.maps.Map(document.getElementById("google__map"), mapOptions);

                map.mapTypes.set('map_style', styledMap);
                map.setMapTypeId('map_style');

                // $('.marker__list li').on('click', function(e){
                //     e.preventDefault();
                //     var lat = $(this).data('lat'),
                //         lng = $(this).data('lng'),
                //         this_index = $('.map__sidebar li').index(this),
                //         newLocation = new google.maps.LatLng(lat, lng);
                //         map.setCenter(newLocation);
                //         map.setZoom(15);
                //         google.maps.event.trigger(marker[this_index], 'click');
                //     if( $(this).hasClass('mobile') ){
                //         let mapTop = $('.map__wrapper').offset().top;
                //         $('html, body').animate({
                //              scrollTop: mapTop - $('header').height()
                //         }, 300);
                //     }
                // });

                $('.marker__list li').each(function() {
                    var mark_lat = $(this).attr('data-lat'),
                        mark_lng = $(this).attr('data-lng'),
                        this_index = $('.marker__list li').index(this),
                        mark_name = 'template_marker_' + this_index,
                        mark_locat = new google.maps.LatLng(mark_lat, mark_lng),
                        mark_str = $(this).attr('data-text');
                    addMarker(mark_locat, this_index, mark_name, mark_str);
                });

                // if( urlParams.has('stop') ){
                //     let stopId = urlParams.get('stop');
                //     $('.map__sidebar li').each(function(){
                //         let thisId = $(this).data('id');
                //         if(stopId == thisId) $(this).trigger('click');
                //     });
                // }
            }

            if ($('.map__block').length) {
                initialize();
            }
        });

        $('.text a[href^="#tab"]').on('click', function(){
            let link = $(this).attr('href'),
                tab = link.split('?')[0].replace(/#tab-/i, ''),
                block = link.split('?')[1].replace(/block=/i, '');

            if( tab && block ){
                let tabNav = $('.tabs__nav li#' + tab + '-nav'),
                    element = $('#'+ block);

                if( tabNav ) tabNav.trigger('click');

                if( element ) {
                    setTimeout(function(){
                        window.scrollTo({
                            top: element.offset().top - 140,
                            left: 0,
                            behavior: 'smooth'
                        });
                    }, 100);
                }
            }
        });
	    
        AOS.init();
    });
})(jQuery);