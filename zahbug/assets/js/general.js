'use strict';

class GeneralClass{
    constructor(){
        this.page = '';
        this.allCategories = false;

        this.init();
    }

    init(){
    	this.popups();
        this.servicesPopups();
        this.documentReady();
    }

    documentReady(){
        document.addEventListener('DOMContentLoaded', function(){
            zahbug.openStreetMapInit();
        });
    }

    openStreetMapInit(){
        if( !document.getElementById('srm-map') ) return false;
        
        let mapLat = document.getElementById('srm-map').dataset.lat,
            mapLng = document.getElementById('srm-map').dataset.lng,
            mapZoom = document.getElementById('srm-map').dataset.zoom,
            mainInfoBlock = document.getElementById('main-information'),
            addInfoBlock = document.getElementById('add-information'),
            homeBtn = document.getElementById('srm-map').querySelector('.srm-home-button'),
            VectorSource = ol.source.Vector,
            VectorTileSource = ol.source.VectorTile,
            GeoJSON = ol.format.GeoJSON,
            Style = ol.style.Style,
            Stroke = ol.style.Stroke,
            Fill = ol.style.Fill,
            Text = ol.style.Text,
            Feature = ol.Feature,
            Point = ol.geom.Point,
            Icon = ol.style.Icon,
            {Tile, Vector, VectorTile} = ol.layer,
            VectorLayer = Vector,
            VectorTileLayer = ol.layer.VectorTile,
            mapLayer = new ol.layer.Tile({source: new ol.source.OSM()}),
            srmMap = new ol.Map({
                target: 'srm-map',
                layers: [ mapLayer ],
                view: new ol.View({
                    center: ol.proj.fromLonLat([mapLng, mapLat]),
                    zoom: mapZoom
                })
            }),
            layerStyle = function (feature) {
                return new Style({
                    stroke: new Stroke({
                        color: 'rgba('+ feature.values_.layerColor+',1)',
                        width: 1,
                    }),
                    fill: new Fill({
                        color: 'rgba('+ feature.values_.layerColor+',0.5)'
                    }),
                    text: new Text({
                        font: 'bold 11px "Montserrat", "Arial Unicode MS", "sans-serif"',
                        fill: new Fill({
                          color: 'black',
                        }),
                        text: feature.values_.label,
                    }),
                })  
            },
            highlightLayerStyle = function (feature) {
                return new Style({
                    stroke: new Stroke({
                        color: 'rgba('+ feature.values_.layerColor+',1)',
                        width: 1,
                    }),
                    fill: new Fill({
                        color: 'rgba('+ feature.values_.layerColor+',0.7)'
                    }),
                    text: new Text({
                        font: 'bold 11px "Montserrat", "Arial Unicode MS", "sans-serif"',
                        fill: new Fill({
                          color: 'black',
                        }),
                        text: feature.values_.label,
                    }),
                })  
            },
            srmCollection, vectorSource, vectorLayer, srmRegionsLayer, srmDistrictLayer, srmUTCLayer;
        taxYearsBlocks();
        showRegionsLayer();
        showMarkers();
        // showDistrictsLayer();
        // showUTCSLayer();
        let selected = null, 
            setStyle = ol.Feature.setStyle,
            mapCurrentZoom,
            selectedLayer, layerType, layerChild, layerHtml, layerExtent;

        srmMap.on( 'pointermove', function (e) {  
            if (selected !== null) {
                if( selected.values_.name != 'Marker' ){
                    selected.setStyle(layerStyle);
                    selected = null;
                }
            }

            srmMap.forEachFeatureAtPixel(e.pixel, function (f) {
                selected = f;
                if( selected.values_.name != 'Marker' ){
                    f.setStyle(highlightLayerStyle);
                }
                return true;
            });
        });

        srmMap.on( 'click', function (e) {
            srmMap.forEachFeatureAtPixel(e.pixel, function (f) {
                layerType = f.values_.type;
                layerHtml = f.values_.html;
                layerChild = f.values_.children;
                layerExtent = f.getGeometry().getExtent();
                addInfoBlock.innerHTML = layerHtml;

                srmMap.getView().fit(layerExtent);
                removeLayer();
                if( layerChild ){
                    showLayers(layerType, layerChild);
                    removeTaxFunction();
                    taxYearsBlocks();
                } else {
                    showLayer(f);
                    removeTaxFunction();
                    taxYearsBlocks();
                }
            });
        });

        srmMap.on( 'moveend', function (e) {
            mapCurrentZoom = srmMap.getView().getZoom();
           
            if( mapCurrentZoom < 8 ){
                mainInfoBlock.classList.remove('hide');
                addInfoBlock.classList.add('hide');
                removeLayer();
                showRegionsLayer();
            } else {
                mainInfoBlock.classList.add('hide');
                addInfoBlock.classList.remove('hide');
            }
            showMarkers();
        });

        homeBtn.addEventListener('click', () => {
            srmMap.getView().animate({
                center: ol.proj.fromLonLat([mapLng, mapLat]),
                zoom: mapZoom
            });
        });

        function removeLayer(){
            srmMap.setLayerGroup(new ol.layer.Group());
            srmMap.addLayer(mapLayer);
        }

        function showMarkers(){
            let markers = document.querySelectorAll('.srm-marker');

            if( !markers ) return false;
            let lat, lng, icon, point, vectorSource, vectorLayer, srmLayer;

            markers.forEach( (marker) => {
                lat = marker.dataset.lat;
                lng = marker.dataset.lng;
                icon = marker.dataset.marker;

                point = new Feature({
                    geometry: new Point(ol.proj.fromLonLat([lng, lat])),
                    name: 'Marker'
                });

                point.setStyle(
                    new Style({
                        image: new Icon({
                            crossOrigin: 'anonymous',
                            src: icon,
                        }),
                    })
                );

                vectorSource = new VectorSource({
                    features: [point],
                }),
                vectorLayer = new VectorLayer({
                    source: vectorSource,
                    style: layerStyle
                }),
                srmLayer = srmMap.addLayer(vectorLayer);
            });


        }

        function showLayer(feature){
            let layersCollecton = {
                'type': 'FeatureCollection',
                'name': 'layer',
                'crs': { 'type': 'name', 'properties': { 'name': 'EPSG:3857' } },
                'features': [
                    { "type": "Feature", "properties": feature.values_ }
                ]
            },
            layerStyle = function (feature) {
                return new Style({
                    stroke: new Stroke({
                        color: 'rgba('+ feature.values_.layerColor+',1)',
                        width: 1,
                    }),
                    fill: new Fill({
                        color: 'rgba('+ feature.values_.layerColor+',0.5)'
                    }),
                    text: new Text({
                        font: 'bold 11px "Montserrat", "Arial Unicode MS", "sans-serif"',
                        fill: new Fill({
                          color: 'black',
                        }),
                        text: feature.values_.label,
                    }),
                })  
            },
            vectorSource, vectorLayer, srmLayer;

            vectorSource = new VectorSource({
                features: new GeoJSON().readFeatures(layersCollecton),
            }),
            vectorLayer = new VectorLayer({
                source: vectorSource,
                style: layerStyle
            }),
            srmLayer = srmMap.addLayer(vectorLayer);
        }

        function showLayers(type, layersIDs){
            let layersCollecton = {
                'type': 'FeatureCollection',
                'name': 'regions',
                'crs': { 'type': 'name', 'properties': { 'name': 'EPSG:3857' } },
                'features': []
            },
            layers = layersIDs.split(', '),
            layerStyle = function (feature) {
                return new Style({
                    stroke: new Stroke({
                        color: 'rgba('+ feature.values_.layerColor+',1)',
                        width: 1,
                    }),
                    fill: new Fill({
                        color: 'rgba('+ feature.values_.layerColor+',0.5)'
                    }),
                    text: new Text({
                        font: 'bold 11px "Montserrat", "Arial Unicode MS", "sans-serif"',
                        fill: new Fill({
                          color: 'black',
                        }),
                        text: feature.values_.label,
                    }),
                })  
            },
            objectID, vectorSource, vectorLayer, srmLayer;
            
            switch ( type ){
                case 'regions':
                    srmDistrict.forEach( (object) => {
                        objectID = object.properties.objectID;
                        if( layers.includes(objectID) ) layersCollecton.features.push(object);
                    });
                    break;
                case 'districts':
                    srmUTC.forEach( (object) => {
                        objectID = object.properties.objectID;
                        if( layers.includes(objectID) ) layersCollecton.features.push(object);
                    });
                    break;
                case 'utcs':
                    srmVC.forEach( (object) => {
                        objectID = object.properties.objectID;
                        if( layers.includes(objectID) ) layersCollecton.features.push(object);
                    });
                    console.log(1);
                    break;
                case 'vcs':
                    console.log(2);
                    break;
            }

            vectorSource = new VectorSource({
                features: new GeoJSON().readFeatures(layersCollecton),
            }),
            vectorLayer = new VectorLayer({
                source: vectorSource,
                style: layerStyle
            }),
            srmLayer = srmMap.addLayer(vectorLayer);
        }

        function showRegionsLayer(){
            if( srmRegions ){
                srmCollection = {
                    'type': 'FeatureCollection',
                    'name': 'regions',
                    'crs': { 'type': 'name', 'properties': { 'name': 'EPSG:3857' } },
                    'features': srmRegions
                },
                vectorSource = new VectorSource({
                    features: new GeoJSON().readFeatures(srmCollection),
                }),
                vectorLayer = new VectorLayer({
                    source: vectorSource,
                    style: layerStyle
                }),
                srmRegionsLayer = srmMap.addLayer(vectorLayer);
            }
        }

        function showDistrictsLayer(){
            if( srmDistrict ){
                srmCollection = {
                    'type': 'FeatureCollection',
                    'name': 'district',
                    'crs': { 'type': 'name', 'properties': { 'name': 'EPSG:3857' } },
                    'features': srmDistrict
                },
                vectorSource = new VectorSource({
                    features: new GeoJSON().readFeatures(srmCollection),
                }),
                vectorLayer = new VectorLayer({
                    source: vectorSource,
                    style: layerStyle
                }),
                srmDistrictLayer = srmMap.addLayer(vectorLayer);
            }
        }

        function showUTCSLayer(){
            if( srmUTC ){
                srmCollection = {
                    'type': 'FeatureCollection',
                    'name': 'otg',
                    'crs': { 'type': 'name', 'properties': { 'name': 'EPSG:3857' } },
                    'features': srmUTC
                },
                vectorSource = new VectorSource({
                    features: new GeoJSON().readFeatures(srmCollection),
                }),
                vectorLayer = new VectorLayer({
                    source: vectorSource,
                    style: layerStyle
                }),
                srmUTCLayer = srmMap.addLayer(vectorLayer);
            }
        }

        function showVCLayer(){
            if( srmVC ){
                srmCollection = {
                    'type': 'FeatureCollection',
                    'name': 'otg',
                    'crs': { 'type': 'name', 'properties': { 'name': 'EPSG:3857' } },
                    'features': srmVC
                },
                vectorSource = new VectorSource({
                    features: new GeoJSON().readFeatures(srmCollection),
                }),
                vectorLayer = new VectorLayer({
                    source: vectorSource,
                    style: layerStyle
                }),
                srmUTCLayer = srmMap.addLayer(vectorLayer);
            }
        }

        function removeTaxFunction(){
            let years = document.querySelectorAll('#main-information .srm-sidebar__year h6');

            if(!years) return false;

            years.forEach( (year) => {
                year.removeEventListener('click', () => {
                    let block = year.closest('.srm-sidebar__year');
                    block.classList.toggle('active');
                });
            });
        }

        function taxYearsBlocks(){
            let years = document.querySelectorAll('.srm-sidebar__year h6');

            if(!years) return false;

            years.forEach( (year) => {
                year.addEventListener('click', () => {
                    let block = year.closest('.srm-sidebar__year');
                    block.classList.toggle('active');
                });
            });
        }
    }

    chooseLocation(id, item){
        let currentItem   = item.closest('.vacancies__nav').querySelector('.value'),
            locationsList = item.closest('ul').querySelectorAll('li'),
            locationsRows = item.closest('.vacancies__section').querySelectorAll('.vacancy__row'),
            locationId, chooseID;

        locationsList.forEach(function(location){
            location.classList.remove('active');
        });

        currentItem.textContent = item.querySelector('span').textContent;

        if( !id ){
            locationsRows.forEach(function(location){
                location.classList.remove('hide');
            });

        } else {
            chooseID = parseInt(id);
            locationsRows.forEach(function(location){
                locationId = parseInt(location.dataset.id);

                if( locationId == chooseID ){
                    location.classList.remove('hide');
                } else {
                    location.classList.add('hide');
                }
            });
        }

        item.classList.add('active');
    }

    showTab(tab){
        let tabsNav = Array.prototype.slice.call(tab.closest('ul').children),
            tabsBlock = tab.closest('.tabs').querySelectorAll('.tabs__block .tab'),
            indexTab = tabsNav.indexOf(tab);

        tabsNav.forEach(function(item){
            item.classList.remove('active');
        });

        tabsBlock.forEach(function(item){
            item.classList.remove('active');
        });

        tab.classList.add('active');
        tabsBlock[indexTab].classList.add('active');
    }

    vacancyShow(button){
    	let vacancyBlock = button.closest('.vacancy__block');

    	if( vacancyBlock.classList.contains('show') ){
    		vacancyBlock.classList.remove('show');
    	} else {
    		vacancyBlock.classList.add('show');
    	}
    }

    faqShow(button){
        let text = button.closest('.guestion__block');

        if( text.classList.contains('show') ){
            text.classList.remove('show');
        } else {
            text.classList.add('show');
        }
    }

    loadPosts(maxPages, currentPage, category = '', button){
        let xhr              = new XMLHttpRequest(),
            articleWrapper   = button.closest('.blog__section').querySelector('.posts__row'); 

        this.page = ( !this.page ) ? ++currentPage : ++this.page;

        articleWrapper.classList.add('load');
        button.classList.add('load');

        xhr.open('POST', ajaxurl, true)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');
        xhr.onload = function () {
            articleWrapper.classList.remove('load');
            button.classList.remove('load');
            let res = this.response;

            articleWrapper.insertAdjacentHTML('beforeEnd', res);
            if( maxPages <= zahbug.page ) button.remove();
            AOS.init();
        };
        xhr.onerror = function() {
            console.log('connection error');
        };
        xhr.send('action=load_posts&page=' + zahbug.page + '&category=' + category );
    }

    loadArticles(maxPages, currentPage, button){
        let xhr              = new XMLHttpRequest(),
            articleWrapper   = button.closest('.blog__section').querySelector('.posts__row'); 

        this.page = ( !this.page ) ? ++currentPage : ++this.page;

        articleWrapper.classList.add('load');
        button.classList.add('load');

        xhr.open('POST', ajaxurl, true)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');
        xhr.onload = function () {
            articleWrapper.classList.remove('load');
            button.classList.remove('load');
            let res = this.response;

            articleWrapper.insertAdjacentHTML('beforeEnd', res);
            if( maxPages <= zahbug.page ) button.remove();
            AOS.init();
        };
        xhr.onerror = function() {
            console.log('connection error');
        };
        xhr.send('action=load_posts&page=' + zahbug.page );
    }

    popups(){
        let popupWrp = document.querySelector('.popup__wrapper'),
            popupsBlocks = document.querySelectorAll('.popup__block'),
            callPopupBtn = document.querySelector('.form__popup'),
            callPopup = document.querySelector('.form__popup__block');

        if(popupWrp){
            popupWrp.addEventListener('click', () => {

                popupsBlocks.forEach( (block) => {
                    block.classList.remove('show');
                });

                popupWrp.classList.remove('show');
            });
        }

        if(popupsBlocks){
            popupsBlocks.forEach( (popupBlock) => {
                let close = popupBlock.querySelector('.close');

                close.addEventListener('click', () => {
                    popupBlock.classList.remove('show');
                    popupWrp.classList.remove('show');
                });
            });
        }
        if(callPopupBtn){
            callPopupBtn.addEventListener('click', () => {
                callPopup.classList.add('show');
            });
            callPopup.querySelector('.close').addEventListener('click', () => {
                callPopup.classList.remove('show');
            });
        }
    }

    servicesPopups(){
        let servicesPopups = document.querySelectorAll('.service__thumbnail.show__popup');
        
        if( servicesPopups ){
            servicesPopups.forEach( (block) => {
                block.addEventListener('click', () => {
                    let serviceId = block.dataset.id,
                        xhr       = new XMLHttpRequest(),
                        formData  = new FormData(),
                        popupWrp  = document.querySelector('.popup__wrapper'),
                        popup     = document.querySelector('.service__popup');

                    popup.querySelector('.body').textContent = '';
                    formData.append( 'action', 'get_service_popup' );
                    formData.append( 'id', serviceId );

                    xhr.open('POST', ajaxurl, true);
                    xhr.onload = function () {
                        if (this.status >= 200 && this.status < 400) {
                            popup.querySelector('.body').insertAdjacentHTML('afterbegin', this.response);
                            popup.classList.add('show');
                            popupWrp.classList.add('show');
                        } else if( this.status >= 400 && this.status < 500 ){

                        }
                    }
                    xhr.onerror = function() {
                        console.log('connection error');
                    };
                    xhr.send(formData);
                });
            });
        }
    }
}

let zahbug = new GeneralClass();