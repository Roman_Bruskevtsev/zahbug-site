<?php
/**
 * Template Name: SRM Page
 *
 * @package WordPress
 * @subpackage NESK
 * @since 1.0
 * @version 1.0
 */

get_header(); 
$args = array(
    'post_type'     => 'srm-object',
    'post_status'   => 'publish',
    'posts_per_page'=> -1
);
$query = new WP_Query( $args );
if ( $query->have_posts() ) { ?>
    <script type="text/javascript">
        let srmRegions = [],
            srmDistrict = [],
            srmUTC = [],
            srmVC = [],
            regionParams, districtParams, utcParams, vcParams;
    <?php while ( $query->have_posts() ) {
        $query->the_post();
        $object_label = get_the_title();
        $object_type = get_field('object_type');
        $object_coordinates = get_field('geometry_coordinates');
        $objects_color = get_field('layer_color') ? get_field('layer_color') : '#4B9220';
        $html = '<h5>'.get_the_title().'</h5>';
        $tax_information = get_field('tax_information');

        if( $tax_information ){
            $html .= '<h5>'.__('Tax deductions, UAH', 'zahbug').'</h5>';
            $html .= '<div class="srm-sidebar__years">';
            foreach( $tax_information as $year ) {
                $html .= '<div class="srm-sidebar__year">';
                    if( $year['year'] ) $html .= '<h6>'.$year['year'].'</h6>';
                    if( $year['tax_group'] ) {
                        foreach( $year['tax_group'] as $group ) {
                            $html .= '<div class="srm-sidebar__tax__group">';
                                if( $group['sum_of_all_taxes'] ) $html .= '<span class="sum">'.$group['sum_of_all_taxes'].'</span>';
                                if( $group['explanation_of_taxes'] ) $html .= '<span class="expl">'.$group['explanation_of_taxes'].'</span>';
                                if( $group['extra_information'] ) {
                                    $html .= '<div class="srm-sidebar__tax__extra">';
                                        foreach( $group['extra_information'] as $extra ){
                                            $html .= '<div>';
                                                if( $extra['sum_of_taxes'] ) $html .= '<span class="sum">'.$extra['sum_of_taxes'].'</span>';
                                                if( $extra['explanation_of_taxes'] ) $html .= '<span class="expl">'.$extra['explanation_of_taxes'].'</span>';
                                            $html .= '</div>';
                                        }
                                    $html .= '</div>';
                                }
                            $html .= '</div>';
                        }
                    }
                $html .= '</div>';
            }
            $html .= '</div>';
        } else {
            $html .= '<h6>'.__('No data available', 'zahbug').'</h6>';
        }
        $parent = wp_get_post_parent_id( get_the_ID() ) ? wp_get_post_parent_id( get_the_ID() ) : '';
        $children_array = '';
        $children = get_children(array(
            'post_parent'   => get_the_ID(),
            'post_type'     => 'srm-object',
            'numberposts'   => -1,
            'post_status'   => 'publish'
        ));
        if( $children ){
            foreach( $children as $child ) {
                if( $children_array == '' ) {
                    $children_array = $child->ID;
                } else {
                    $children_array = $children_array.', '.$child->ID;
                }
            }
        }
        
        switch ( $object_type ) {
            case '0': ?>
                regionParams = <?php echo $object_coordinates; ?>;
                regionParams.properties.label = '<?php echo $object_label; ?>';
                regionParams.properties.layerColor = '<?php echo $objects_color; ?>';
                regionParams.properties.html = '<?php echo $html; ?>';
                regionParams.properties.objectID = '<?php echo get_the_ID(); ?>';
                regionParams.properties.type = 'regions';
                regionParams.properties.children = '<?php echo $children_array; ?>';

                srmRegions.push(regionParams);
                <?php break;
            case '1': ?>
                districtParams = <?php echo $object_coordinates; ?>;
                districtParams.properties.label = '<?php echo $object_label; ?>';
                districtParams.properties.layerColor = '<?php echo $objects_color; ?>';
                districtParams.properties.html = '<?php echo $html; ?>';
                districtParams.properties.objectID = '<?php echo get_the_ID(); ?>';
                districtParams.properties.type = 'districts';
                districtParams.properties.parent = '<?php echo $parent; ?>';
                districtParams.properties.children = '<?php echo $children_array; ?>';

                srmDistrict.push(districtParams);
                <?php break;
            case '2': ?>
                utcParams = <?php echo $object_coordinates; ?>;
                utcParams.properties.label = '<?php echo $object_label; ?>';
                utcParams.properties.layerColor = '<?php echo $objects_color; ?>';
                utcParams.properties.html = '<?php echo $html; ?>';
                utcParams.properties.objectID = '<?php echo get_the_ID(); ?>';
                utcParams.properties.type = 'utcs';
                utcParams.properties.parent = '<?php echo $parent; ?>';
                utcParams.properties.children = '<?php echo $children_array; ?>';

                srmUTC.push(utcParams);
                <?php break;
            case '3': ?>
                vcParams = <?php echo $object_coordinates; ?>;
                vcParams.properties.label = '<?php echo $object_label; ?>';
                vcParams.properties.layerColor = '<?php echo $objects_color; ?>';
                vcParams.properties.html = '<?php echo $html; ?>';
                vcParams.properties.objectID = '<?php echo get_the_ID(); ?>';
                vcParams.properties.type = 'vcs';
                vcParams.properties.parent = '<?php echo $parent; ?>';

                srmVC.push(vcParams);
                <?php break;
            default:
                // code...
                break;
        }
    } ?>
    </script>
<?php } wp_reset_postdata(); 
$lat = get_field('map_latitude') ? get_field('map_latitude') : '';
$lng = get_field('map_longitude') ? get_field('map_longitude') : '';
$zoom = get_field('map_zoom') ? get_field('map_zoom') : '';
$office_lat = get_field('main_office_latitude') ? get_field('main_office_latitude') : '';
$office_lng = get_field('main_office_longitude') ? get_field('main_office_longitude') : '';
$office_address = get_field('main_office_address') ? get_field('main_office_address') : '';
$sidebar = get_field('sidebar'); ?>

<section class="srm-map__section">
    <?php if( get_field('markers') ) { ?>
    <div class="srm-markers">
        <?php foreach ( get_field('markers') as $marker) { ?>
            <div class="srm-marker" data-lat="<?php echo $marker['latitude']; ?>" data-lng="<?php echo $marker['longitude']; ?>" data-marker="<?php echo $marker['marker']; ?>"></div>
        <?php } ?>
    </div>
    <?php } ?>
    <div class="srm-map" id="srm-map" data-lat="<?php echo $lat; ?>" data-lng="<?php echo $lng; ?>" data-zoom="<?php echo $zoom; ?>">
        <div class="ol-control srm-home-button">
            <button title="<?php _e('Home', 'zahbug'); ?>"></button>
        </div>
    </div>
    <div class="srm-sidebar__wrapper">
        <div class="srm-sidebar__block" id="main-information">
            <?php if( $sidebar['title'] ) { ?>
                <h5><?php echo $sidebar['title']; ?></h5>
            <?php } 
            if( $sidebar['subtitle'] ) { ?>
                <h5><?php echo $sidebar['subtitle']; ?></h5>
            <?php } 
            if( $sidebar['years'] ) { ?>
                <div class="srm-sidebar__years">
                <?php foreach ( $sidebar['years'] as $year ) { ?>
                    <div class="srm-sidebar__year">
                    <?php if( $year['year'] ) { ?>
                        <h6><?php echo $year['year'] ?></h6>
                    <?php } 
                    if( $year['tax_group'] ) { 
                        foreach ( $year['tax_group'] as $tax_group ) { ?>
                        <div class="srm-sidebar__tax__group">
                            <?php if( $tax_group['sum_of_all_taxes'] ) { ?>
                                <span class="sum"><?php echo $tax_group['sum_of_all_taxes']; ?></span>
                            <?php } 
                            if( $tax_group['explanation_of_taxes'] ) { ?>
                                <span class="expl"><?php echo $tax_group['explanation_of_taxes']; ?></span>
                            <?php } 
                            if( $tax_group['extra_information'] ) { ?>
                                <div class="srm-sidebar__tax__extra">
                                    <?php foreach ( $tax_group['extra_information'] as $extra ) { ?>
                                        <div>
                                        <?php if( $extra['sum_of_taxes'] ) { ?>
                                            <span class="sum"><?php echo $extra['sum_of_taxes']; ?></span>
                                        <?php } 
                                        if( $extra['explanation_of_taxes'] ) { ?>
                                            <span class="expl"><?php echo $extra['explanation_of_taxes']; ?></span>
                                        <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <?php }
                    } ?>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="srm-sidebar__block hide" id="add-information">
            
        </div>
    </div>
</section>

<?php get_footer();