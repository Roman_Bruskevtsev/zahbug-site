<?php
/**
 * 
 */
class ZahbugClass {
	const SCRIPTS_VERSION    = '1.0.14';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_footer',  array( $this, 'js_variables' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init') );
		add_action( 'init', array( $this, 'custom_posts_type') );
		add_action( 'init', array( $this, 'custom_taxonomy') );
		
		add_filter( 'upload_mimes', array( $this, 'enable_svg_types'), 99 );

		add_action( 'wp_ajax_load_posts', array( $this, 'load_posts' ) );
        add_action( 'wp_ajax_nopriv_load_posts', array( $this, 'load_posts' ) );
        add_action( 'wp_ajax_get_service_popup', array( $this, 'get_service_popup' ) );
        add_action( 'wp_ajax_nopriv_get_service_popup', array( $this, 'get_service_popup' ) );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'zahbug-css', $this->stylesDir.'/main.min.css' , '', self::SCRIPTS_VERSION);
		// wp_enqueue_style( 'leaflet', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css' , '', '');
		// wp_enqueue_style( 'mapbox', 'https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' , '', '');
		wp_enqueue_style( 'openLayers', 'https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/css/ol.css' , '', '');
    	wp_enqueue_style( 'zahbug-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	// wp_enqueue_script( 'leaflet', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	// wp_enqueue_script( 'mapbox', 'https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js', array( 'jquery' ), '', true );
    	wp_enqueue_script( 'os-polyfill', 'https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList', array( 'jquery' ), '', true );
    	wp_enqueue_script( 'openLayers', 'https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/build/ol.js', array( 'jquery' ), '', true );
    	wp_enqueue_script( 'all-js', $this->scriptsDir.'/all.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
        
    }

    public function theme_setup(){
    	load_theme_textdomain( 'zahbug' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );
	    add_theme_support( 'post-formats', array( 'video', 'audio' ) );

	    add_image_size( 'service-thumbnails', 360, 160, true );
	    add_image_size( 'new-thumbnails', 360, 240, true );
	    add_image_size( 'product-thumbnails', 848, 436, true );

	    register_nav_menus( array(
	        'main'          => __( 'Main Menu', 'zahbug' ),
	        'right-menu'    => __( 'Right Navigation Menu', 'zahbug' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'zahbug'),
		        'menu_title'    => __('Theme Settings', 'zahbug'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1', 'zahbug' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'zahbug' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 2', 'zahbug' ),
	        'id'            => 'footer-2',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'zahbug' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 3', 'zahbug' ),
	        'id'            => 'footer-3',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'zahbug' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>',
	        	asyncUpload = '<?php echo admin_url('async-upload.php'); ?>';
	    </script>
	<?php }

	public function custom_posts_type(){
		$services_labels = array(
			'name'					=> __('Services', 'zahbug'),
			'singular_name'			=> __('Service', 'zahbug'),
			'add_new'				=> __('Add Service', 'zahbug'),
			'add_new_item'			=> __('Add New Service', 'zahbug'),
			'edit_item'				=> __('Edit Service', 'zahbug'),
			'new_item'				=> __('New Service', 'zahbug'),
			'view_item'				=> __('View Service', 'zahbug')
		);

		$services_args = array(
			'label'               => __('Services', 'zahbug'),
			'description'         => __('Service information page', 'zahbug'),
			'labels'              => $services_labels,
			'supports'            => array( 'title', 'thumbnail', 'page-attributes'),
			'taxonomies'          => array( '' ),
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => false,
			'can_export'          => true,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'query_var'           => true,
			'rewrite'             => '',
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-buddicons-topics'
		);

		register_post_type( 'service', $services_args );

		$services_labels = array(
			'name'					=> __('Articles', 'zahbug'),
			'singular_name'			=> __('Article', 'zahbug'),
			'add_new'				=> __('Add Article', 'zahbug'),
			'add_new_item'			=> __('Add New Artcile', 'zahbug'),
			'edit_item'				=> __('Edit Article', 'zahbug'),
			'new_item'				=> __('New Article', 'zahbug'),
			'view_item'				=> __('View Article', 'zahbug')
		);

		$services_args = array(
			'label'               => __('Articles', 'zahbug'),
			'description'         => __('Article information page', 'zahbug'),
			'labels'              => $services_labels,
			'supports'            => array( 'title', 'excerpt', 'post-formats'),
			'taxonomies'          => array( '' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => false,
			'can_export'          => true,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'query_var'           => true,
			'rewrite'             => '',
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-admin-page'
		);
		register_post_type( 'article', $services_args );

		$services_labels = array(
			'name'					=> __('Vacancies', 'zahbug'),
			'singular_name'			=> __('Vacancy', 'zahbug'),
			'add_new'				=> __('Add Vacancy', 'zahbug'),
			'add_new_item'			=> __('Add New Vacancy', 'zahbug'),
			'edit_item'				=> __('Edit Vacancy', 'zahbug'),
			'new_item'				=> __('New Vacancy', 'zahbug'),
			'view_item'				=> __('View Vacancy', 'zahbug')
		);

		$services_args = array(
			'label'               => __('Vacancies', 'zahbug'),
			'description'         => __('Vacancy information page', 'zahbug'),
			'labels'              => $services_labels,
			'supports'            => array( 'title', 'editor' ),
			'taxonomies'          => array( '' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => true,
			'can_export'          => true,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => false,
			'exclude_from_search' => false,
			'query_var'           => true,
			'rewrite'             => '',
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-buddicons-buddypress-logo'
		);
		register_post_type( 'vacancy', $services_args );

		$services_labels = array(
			'name'					=> __('Products', 'zahbug'),
			'singular_name'			=> __('Product', 'zahbug'),
			'add_new'				=> __('Add Product', 'zahbug'),
			'add_new_item'			=> __('Add New Product', 'zahbug'),
			'edit_item'				=> __('Edit Product', 'zahbug'),
			'new_item'				=> __('New Product', 'zahbug'),
			'view_item'				=> __('View Product', 'zahbug')
		);

		$services_args = array(
			'label'               => __('Products', 'zahbug'),
			'description'         => __('Product information page', 'zahbug'),
			'labels'              => $services_labels,
			'supports'            => array( 'title', 'thumbnail' ),
			'taxonomies'          => array( '' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => true,
			'can_export'          => true,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'query_var'           => true,
			'rewrite'             => '',
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-carrot'
		);
		register_post_type( 'product', $services_args );

		$services_labels = array(
			'name'					=> __('SRM objects', 'zahbug'),
			'singular_name'			=> __('SRM object', 'zahbug'),
			'add_new'				=> __('Add SRM object', 'zahbug'),
			'add_new_item'			=> __('Add New SRM object', 'zahbug'),
			'edit_item'				=> __('Edit SRM object', 'zahbug'),
			'new_item'				=> __('New SRM object', 'zahbug'),
			'view_item'				=> __('View SRM object', 'zahbug')
		);

		$services_args = array(
			'label'               => __('SRM object', 'zahbug'),
			'description'         => __('SRM object information page', 'zahbug'),
			'labels'              => $services_labels,
			'supports'            => array( 'title', 'excerpt', 'page-attributes' ),
			'taxonomies'          => array( '' ),
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => false,
			'can_export'          => true,
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => false,
			'query_var'           => false,
			'rewrite'             => '',
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-admin-site'
		);
		register_post_type( 'srm-object', $services_args );
	}

	public function custom_taxonomy(){
		$taxonomy_labels = array(
			'name'                        => __('Vacancies locations','zahbug'),
			'singular_name'               => __('Vacancy location','zahbug'),
			'menu_name'                   => __('Vacancies locations','zahbug'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'vacancy-location',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'vacancy-location', 'vacancy', $taxonomy_args );
	}

	public function language_switcher(){
		if (!function_exists( 'pll_the_languages')) return;
		$output = '';
		$languages = pll_the_languages(array(
		    'display_names_as'       => 'slug',
		    'hide_if_no_translation' => 1,
		    'raw'                    => true
		));

		$output .= '<div class="language__switcher float-left">';
			foreach ($languages as $lang) {
				if($lang['current_lang']) {
					$output .= '<span>'.$lang['name'].'</span>';
				}
			}
			$output .= '<ul>';
			foreach ($languages as $lang) {
				if($lang['current_lang']) {
					$output .= '<li class="current">';
				} else {
					$output .= '<li>';
				}
					$output .= '<a href="'.$lang['url'].'">'.$lang['name'].'</a>';
				$output .= '</li>';
			}
			$output .= '</ul>';
		$output .= '</div>';
		
		return $output;
	}

	public function load_posts(){
		$paged = $_POST['page'];
    	$blog_id = get_option('page_for_posts'); 
    	$posts_per_page = get_field('posts_per_page', $blog_id); 
    	$output = '';
    	$args = array(
			'post_type'			=> 'post',
			'posts_per_page' 	=> $posts_per_page,
			'post_status'		=> 'publish',
			'paged'				=> $paged
		);

		if( $_POST['category'] ) $args['cat'] = $_POST['category'];

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {		
			while ( $query->have_posts() ) { $query->the_post();
				$output .= '<div class="col-lg-12">'.get_template_part( 'template-parts/post/content', 'thumbnail' ).'</div>';
			}
		}
		wp_reset_postdata();
		wp_die();
	}

	public function get_service_popup(){
		$serviceId = $_POST['id'];

		if( have_rows('content', $serviceId ) ){
			while ( have_rows('content', $serviceId ) ) { the_row();
				if( get_row_layout() == 'images_+_text_section' ) {
					$images = get_sub_field('images'); ?>
					<div class="popup__content">
						<div class="container-fluid">
							<div class="row">
							<?php if( $images ){ ?>
								<div class="col-lg-5">
									<div class="images">
										<?php foreach ( $images as $image ) { ?>
										<div class="image">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
										</div>
										<?php } ?>
									</div>
								</div>
							<?php } else { ?>
								<div class="col-lg-5">
									<div class="images">
										<div class="image placeholder"></div>
									</div>
								</div>
							<?php }
							if( get_sub_field('text') ) { ?>
								<div class="col-lg-7">
									<div class="text">
										<?php echo get_sub_field('text'); ?>
									</div>
								</div>
							<?php } ?>
							</div>
						</div>
					</div>
				<?php }
			}
		} else { ?>
			<div class="popup__content">
				<div class="container-fluid">
					<div class="row">
						<div class="col">
							<div class="text"><h5><b><?php _e('Nothing to show', 'zahbug'); ?></b></h5></div>
						</div>
					</div>
				</div>
			</div>
		<?php }

		wp_die();
	}

	// public function load_articles(){
	// 	$paged = $_POST['page'];
 //    	$posts_per_page = get_option('posts_per_page'); 
 //    	$output = '';
 //    	$args = array(
	// 		'post_type'			=> 'post',
	// 		'posts_per_page' 	=> $posts_per_page,
	// 		'post_status'		=> 'publish',
	// 		'paged'				=> $paged
	// 	);

	// 	$query = new WP_Query( $args );

	// 	if ( $query->have_posts() ) {		
	// 		while ( $query->have_posts() ) { $query->the_post();
	// 			$output .= '<div class="col-lg-12">'.get_template_part( 'template-parts/post/content', 'thumbnail' ).'</div>';
	// 		}
	// 	}
	// 	wp_reset_postdata();
	// 	wp_die();
	// }

    public function __return_false() {
        return false;
    }
}

$zahbug = new ZahbugClass();