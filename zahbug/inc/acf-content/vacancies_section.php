<section class="vacancies__section">
	<div class="container">
	<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<h2 class="h3" data-aos="fade-right"><b><?php the_sub_field('title'); ?></b></h2>
			</div>		
		</div>
	<?php } 
	if( get_sub_field('text') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="text" data-aos="fade-right"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
	<?php } 
	$args = array(
		'taxonomy'		=> 'vacancy-location',
		'hide_empty'	=> true
	);
	$locations = get_terms($args);
	$form = get_sub_field('vacancy_form_shortcode'); 
	if( $locations ){ ?>
		<div class="row">
			<div class="col-lg-4">
				<div class="vacancies__nav">
					<span class="value" data-placeholder="<?php _e('Choose region', 'zahbug'); ?>"><?php _e('Choose region', 'zahbug'); ?></span>
					<ul>
						<li class="active" onclick="zahbug.chooseLocation(0, this);"><span><?php _e('All regions', 'zahbug'); ?></span></li>
						<?php foreach ( $locations as $location ) { ?>
							<li onclick="zahbug.chooseLocation(<?php echo $location->term_id; ?>, this);"><span><?php echo $location->name; ?></span></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<?php if( $form ) { ?>
			<div class="col-lg-8">
				<button class="btn show__form"><span class="left"></span><?php _e('Download your own CV', 'zahbug'); ?><span class="right"></span></button>
			</div>
			<?php } ?>
		</div>
	<?php } 
	if( $form ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="vacancy__form"><?php echo do_shortcode($form); ?></div>
			</div>
		</div>
	<?php } ?>
		
		<?php
		$vacancies = get_sub_field('vacancies');
		if( $vacancies ) {
			$args = array(
				'posts_per_page' 	=> -1,
				'post_type' 		=> 'vacancy',
				'post__in'			=> $vacancies,
				'orderby'			=> 'post__in'
			);

			$query = new WP_Query( $args );	
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) { $query->the_post(); ?>
						<?php get_template_part( 'template-parts/vacancy/content', 'thumbnail' ); ?>
				<?php } 
			}
			wp_reset_postdata();
		} ?>
		</div>
	</div>
</section>