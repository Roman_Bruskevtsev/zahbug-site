<section class="products__services__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="tabs">
					<nav class="tabs__nav">
						<ul>
							<li class="active" id="products-nav" onclick="zahbug.showTab(this);"><?php the_sub_field('products_title') ?></li>
							<li id="services-nav" onclick="zahbug.showTab(this);"><?php the_sub_field('title') ?></li>
						</ul>
					</nav>
					<div class="tabs__block">
						<div class="tab active" id="products">
							<?php 
							$products = get_sub_field('choose_products');
							if( $products ) { ?>
							<div class="products__row" id="products-block">
								<?php $args = array(
									'posts_per_page' 	=> -1,
									'post_type' 		=> 'product',
									'post__in'			=> $products,
									'orderby'			=> 'post__in'
								);

								$query = new WP_Query( $args );	
								if ( $query->have_posts() ) {
									$i = 0;
									while ( $query->have_posts() ) { $query->the_post();
										if( $i % 2 == 0 ){ 
											get_template_part( 'template-parts/product/content', 'thumbnail' );
										} else {
											get_template_part( 'template-parts/product/content', 'thumbnail-revert' );
										}
									$i++; } 
								}
								wp_reset_postdata(); ?>
							</div>
							<?php } 
							$originators_show = get_sub_field('show_originators'); 
							if( $originators_show ) { 
								$originators = get_sub_field('originators'); ?>
								<div class="partners__row">
									<?php if( $originators['title'] ) { ?><h3 data-aos="fade-left"><b><?php echo $originators['title']; ?></b></h3><?php } 
									if( $originators['logos'] ) { ?>
										<div class="logos__row">
										<?php foreach ( $originators['logos'] as $logo ) { ?>
											<div class="logo" data-aos="fade-up">
												<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['title']; ?>">
											</div>
										<?php } ?>
										</div>
									<?php } ?>
								</div>
							<?php } 
							$clients_show = get_sub_field('show_clients'); 
							if( $clients_show ) { 
								$clients = get_sub_field('clients'); ?>
								<div class="partners__row">
									<?php if( $clients['title'] ) { ?><h3 data-aos="fade-left"><b><?php echo $clients['title']; ?></b></h3><?php } 
									if( $clients['logos'] ) { ?>
										<div class="logos__row">
										<?php foreach ( $clients['logos'] as $logo ) { ?>
											<div class="logo" data-aos="fade-up">
												<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['title']; ?>">
											</div>
										<?php } ?>
										</div>
									<?php } ?>
								</div>
							<?php } ?>
						</div>
						<div class="tab" id="services">
							<?php 
							$content = get_sub_field('content'); 
							foreach ($content as $section => $value) {
								$id = $value['id'] ? ' id="'.$value['id'].'"' : '';

								$layout = $value["acf_fc_layout"];
								if( $layout == 'services' ) { ?>
									<div class="services__row"<?php echo $id; ?>>
									<?php if( $value['title'] ) { ?>
										<div class="row">
											<div class="col-lg-12">
												<div class="text" data-aos="fade-right"><h5><b><?php echo $value['title']; ?></b></h5></div>
											</div>
										</div>
									<?php } if( $value['choose_services'] ) { ?>
										<div class="row">
											<?php 
											$args = array(
												'posts_per_page' 	=> -1,
												'post_type' 		=> 'service',
												'post__in'			=> $value['choose_services'],
												'orderby'			=> 'post__in'
											); 
											$query = new WP_Query( $args );	
											if ( $query->have_posts() ) {
												while ( $query->have_posts() ) { $query->the_post(); ?>
													<div class="col-md-6 col-lg-4">
														<?php get_template_part( 'template-parts/service/content', 'active-thumbnail' ); ?>
													</div>
												<?php } 
											}
											wp_reset_postdata(); ?>
										</div>
									<?php } ?>
									</div>
								<?php }
								elseif ( $layout == 'documents' ) { ?>
									<div class="documents__row"<?php echo $id; ?>>
										<?php if( $value['title'] ) { ?>
										<div class="row">
											<div class="col-lg-12">
												<div class="text" data-aos="fade-right"><h6><b><?php echo $value['title']; ?></b></h6></div>
											</div>
										</div>
										<?php } 
										$documents = $value['documents']; 
										if( $documents ) { ?>
											<div class="row">
											<?php foreach ( $documents as $document ) { 
												if( $document['url'] ) { ?>
												<div class="col-md-6 col-lg-4">
													<a class="document__block" href="<?php echo $document['url']; ?>" target="_blank" data-aos="fade-up">
														<span class="icon"></span>
														<div class="name">
															<p><?php echo $document['title']; ?></p>
														</div>
													</a>
												</div>
												<?php } 
											}?>
											</div>
										<?php } ?>
									</div>
								<?php }
								elseif ( $layout == 'text' ) { ?>
									<div class="text__columns"<?php echo $id; ?> data-aos="fade-up">
										<div class="row">
											<div class="col-md-6"><div class="text text__1"><?php echo $value['text']; ?></div></div>
											<div class="col-md-6"><div class="text text__2"><?php echo $value['text_2']; ?></div></div>
										</div>
									</div>
								<?php }
								elseif ( $layout == 'benefits' ) { 
									$benefits = $value['benifts']; ?>
									<div class="benefits__row"<?php echo $id; ?>>
										<div class="row">
										<?php
										$i = 1;
										foreach ( $benefits as $benefit ) { ?>
											<div class="col-md-6 col-lg-4">
												<div class="benefit__block">
													<span><?php echo $i; ?></span>
													<div class="text">
														<?php if( $benefit['title'] ) { ?><h6><b><?php echo $benefit['title']; ?></b></h6><?php } ?>
														<?php if( $benefit['text'] ) { ?><p><?php echo $benefit['text']; ?></p><?php } ?>
													</div>
												</div>
											</div>
										<?php $i++; } ?>
										</div>
									</div>
								<?php }
								elseif ( $layout == 'cultures' ) { 
									$cultures = $value['cultures']; ?>
									<div class="cultures__row"<?php echo $id; ?>>
										<?php if( $value['title'] ) { ?>
										<div class="row">
											<div class="col-lg-12">
												<div class="text" data-aos="fade-right"><h6><b><?php echo $value['title']; ?></b></h6></div>
											</div>
										</div>
										<?php } 
										if( $cultures ) { ?>
										<div class="row">
										<?php foreach ($cultures as $culture) { ?>
											<div class="col-md-6 col-lg-3">
												<div class="culture__block" data-aos="fade-up">
													<?php if( $culture['icon'] ){ ?>
													<div class="icon">
														<img src="<?php echo $culture['icon']['url']; ?>" alt="<?php echo $culture['icon']['title']; ?>">
													</div>
													<h6><?php echo $culture['title']; ?></h6>
													<?php } ?>
												</div>
											</div>
										<?php } ?>
										</div>
										<?php } ?>
									</div>
								<?php }
								elseif ( $layout == 'images_texts_section' ) { 
									$blocks = $value['images_texts']; 
									if( $blocks ) { ?>
									<div class="images__texts__row"<?php echo $id; ?>>
										<div class="row">
										<?php foreach ( $blocks as $block ) { 
											$background = $block['image'] ? ' style="background-image: url('.$block['image']['url'].')"' : ''; ?>
											<div class="col-lg-6">
												<div class="content__block">
													<div class="row">
														<div class="col-lg-10">
															<div class="image"<?php echo $background; ?> data-aos="fade-up"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-2"></div>
														<div class="col-lg-10">
															<div class="text" data-aos="fade-left"><?php echo $block['text']; ?></div>
														</div>
													</div>
												</div>
											</div>
										<?php } ?>
										</div>
									</div>
									<?php } 
								}
								elseif ( $layout == 'prices' ) { 
									$prices = $value['prices']; ?>
									<div class="prices__row"<?php echo $id; ?>>
										<?php if( $value['title'] ) { ?>
										<div class="row">
											<div class="col-lg-12">
												<div class="text" data-aos="fade-right"><h6><b><?php echo $value['title']; ?></b></h6></div>
											</div>
										</div>
										<?php } 
										if( $prices ) { ?>
										<div class="row">
										<?php foreach ( $prices as $price ) { ?>
											<div class="col-lg-6">
												<div class="price__block" data-aos="fade-up">
													<?php if( $price['name'] ) { ?>
														<div class="text"><h6><?php echo $price['name']; ?></h6></div>
													<?php } 
													if( $price['price'] ) { ?>
														<div class="price"><?php echo $price['price']; ?></div>
													<?php } ?>
												</div>
											</div>
										<?php } ?>
										</div>
										<?php } ?>
									</div>
								<?php }
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>