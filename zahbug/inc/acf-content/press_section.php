<section class="press__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text" data-aos="fade-right">
				<?php if( get_sub_field('title') ) { ?>
					<h2 class="h3"><b><?php the_sub_field('title'); ?></b></h2>
				<?php } ?>
				</div>
			</div>
		</div>
		<div class="row">
			<?php
			$articles = get_sub_field('articles');
			if( $articles ) {
				$args = array(
					'posts_per_page' 	=> -1,
					'post_type' 		=> 'article',
					'post__in'			=> $articles,
					'orderby'			=> 'post__in'
				);

				$query = new WP_Query( $args );	
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) { $query->the_post(); ?>
						<div class="col-lg-6">
							<?php get_template_part( 'template-parts/article/content', 'thumbnail' ); ?>
						</div>
					<?php } 
				}
				wp_reset_postdata();
			} ?>
		</div>
		<?php 
		$link = get_sub_field('link');
		if($link) { 
			$target = ($link['target']) ? ' target="'.$link['target'].'"' : ''; ?>
			<div class="row text-center" data-aos="fade-up" data-aos-duration="600">
				<div class="col-lg-12">
					<a class="btn" href="<?php echo $link['url']; ?>"<?php echo $target; ?>>
						<span class="left"></span>
						<?php echo $link['title']; ?>
						<span class="right"></span>
					</a>
				</div>
			</div>
		<?php } ?>
	</div>
</section>