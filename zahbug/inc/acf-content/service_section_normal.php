<section class="service__section__normal">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text" data-aos="fade-right"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<?php 
					$show_links = get_sub_field('show_links');
					$services = get_sub_field('services');
					if( $services ) { 
						$args = array(
							'posts_per_page' 	=> -1,
							'post_type' 		=> 'service',
							'post__in'			=> $services,
							'orderby'			=> 'post__in'
						);

						$query = new WP_Query( $args );	
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) { $query->the_post(); ?>
								<div class="col-md-6 col-lg-4">
									<?php if( $show_links ) {
										get_template_part( 'template-parts/service/content', 'thumbnail-link' );
									} else {
										get_template_part( 'template-parts/service/content', 'thumbnail' );
									} ?>
								</div>
							<?php } 
						}
						wp_reset_postdata();
					} ?>
				</div>
			</div>
			
		</div>
	</div>
</section>