<?php 
$text = get_sub_field('text');
if( $text ) { ?>
<section class="content__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text">
					<?php the_sub_field('text'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>