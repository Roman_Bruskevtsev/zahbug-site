<?php $contacts = get_sub_field('contacts'); ?>
<section class="contacts__section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-8">
				<div class="map__block" data-lat="<?php the_sub_field('map_latitude'); ?>" data-lng="<?php the_sub_field('map_longitude'); ?>" data-zoom="<?php the_sub_field('map_zoom'); ?>" data-marker="<?php echo get_theme_file_uri().'/assets/images/icon_marker.png'; ?>" data-aos="fade-left">
					<div class="block" id="google__map"></div>
					<ul class="marker__list">
						<?php foreach ($contacts as $contact) { ?>
							<li data-lat="<?php echo $contact['latitude']; ?>" data-lng="<?php echo $contact['longitude']; ?>" data-text="<?php echo $contact['text']; ?>"></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
	<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-3">
				<h4 data-aos="fade-right"><b><?php the_sub_field('title'); ?></b></h4>
			</div>		
		</div>
	<?php } ?>
	</div>
	<div class="container-fluid">
	<?php if( $contacts ) { ?>
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-10">
				<div class="row contact__row">
				<?php foreach ( $contacts as $contact ) { ?>
					<div class="col-lg-6 col-xl-3">
						<div class="contact__block" data-aos="fade-up">
							<p><?php echo $contact['text']; ?></p>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	<?php } ?>
	</div>
</section>