<section class="contacts__of__departments">
	<div class="container">
		<div class="row">
			<div class="col">
			<?php 
			$departments = get_sub_field('departments');
			if( $departments ) { 
				foreach ( $departments as $department ) { 
					$blocks = $department['blocks']; ?>
					<div class="contact__group">
					<?php if( $department['location'] ) { ?>
						<div class="title"><h5><?php echo $department['location']; ?></h5></div>
					<?php } 
					if( $blocks ) { ?>
						<?php foreach ( $blocks as $block ) { 
							if( $block['details'] ) { ?>
							<div class="contacts__block">
								<div class="details__wrapper"><?php echo $block['details']; ?></div>
								<div class="show__more"></div>
							</div>
							<?php }
						} ?>
					
					<?php } ?>
					</div>
				<?php } 
			} ?>
			</div>
		</div>
	</div>
</section>