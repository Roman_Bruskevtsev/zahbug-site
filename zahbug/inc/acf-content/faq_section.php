<section class="faq__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text" data-aos="fade-right">
				<?php if( get_sub_field('title') ) { ?>
					<h2 class="h5"><b><?php the_sub_field('title'); ?></b></h2>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php if( get_sub_field('questions') ) { 
			$questions = get_sub_field('questions'); 
			foreach( $questions as $question ) { ?>
				<div class="row">
					<div class="col-lg-12">
						<div class="guestion__block">
							<div class="title">
								<?php if( $question['title'] ) { ?><h6><b><?php echo $question['title']; ?></b></h6><?php } ?>
							</div>
							<div class="show__icon" onclick="zahbug.faqShow(this);"></div>
							<div class="text">
								<?php echo $question['text']; ?>
							</div>
						</div>
					</div>
				</div>
			<?php }
		} ?>
	</div>
</section>