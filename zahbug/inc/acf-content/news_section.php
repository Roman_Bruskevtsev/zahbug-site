<section class="news__section">
	<div class="container">
	<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<h2 class="h3" data-aos="fade-right"><b><?php the_sub_field('title'); ?></b></h2>
			</div>		
		</div>
	<?php } 
	$posts_to_show = get_sub_field('posts_to_show');
	$categories = get_sub_field('choose_category');
	$args = array(
		'posts_per_page' 	=> $posts_to_show,
		'post_type' 		=> 'post',
		'post_status'		=> 'publish'
	);

	if( $categories ){
		$args['cat'] = $categories;
	}

	$query = new WP_Query( $args );	
	if ( $query->have_posts() ) { ?>
	<div class="row">
		<?php while ( $query->have_posts() ) { $query->the_post(); ?>
			<div class="col-lg-12">
				<?php get_template_part( 'template-parts/post/content', 'thumbnail' ); ?>
			</div>
		<?php } ?>
	</div>
	<?php }	
	wp_reset_postdata(); 
	$link = get_sub_field('button');
	if($link) { 
		$target = ($link['target']) ? ' target="'.$link['target'].'"' : ''; ?>
		<div class="row text-center" data-aos="fade-up" data-aos-duration="600">
			<div class="col-lg-12">
				<a class="btn" href="<?php echo $link['url']; ?>"<?php echo $target; ?>>
					<span class="left"></span>
					<?php echo $link['title']; ?>
					<span class="right"></span>
				</a>
			</div>
		</div>
	<?php } ?>
	</div>
</section>