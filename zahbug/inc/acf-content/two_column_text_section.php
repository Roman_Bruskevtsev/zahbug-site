<section class="column__text__section">
	<div class="container">
		<div class="row">
			<dov class="col-lg-12">
				<div class="text" data-aos="fade-up"><?php the_sub_field('text'); ?></div>
			</dov>
		</div>
	</div>
</section>