<section class="service__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="text" data-aos="fade-right">
				<?php if( get_sub_field('title') ) { ?>
					<h2 class="h3"><b><?php the_sub_field('title'); ?></b></h2>
				<?php } ?>
				<?php if( get_sub_field('text') ) { ?><p><?php the_sub_field('text'); ?></p><?php } 
				$link = get_sub_field('button');
				if( $link ) { 
					$target = $link['target'] ? ' target="_blank"' : ''; ?>
					<a href="<?php echo $link['url']; ?>" class="btn"<?php echo $target; ?>>
						<span class="left"></span>
						<?php echo $link['title']; ?>
						<span class="right"></span>
					</a>
				<?php } ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3"></div>
			<div class="col-lg-9">
				<div class="row service__row">
					<?php 
					$services = get_sub_field('services');
					if( $services ) { 
						$args = array(
							'posts_per_page' 	=> -1,
							'post_type' 		=> 'service',
							'post__in'			=> $services,
							'orderby'			=> 'post__in'
						);

						$query = new WP_Query( $args );	
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) { $query->the_post(); ?>
								<div class="col-md-6 col-lg-5">
									<?php get_template_part( 'template-parts/service/content', 'thumbnail' ); ?>
								</div>
							<?php } 
						}
						wp_reset_postdata();
					} ?>
				</div>
			</div>
			
		</div>
	</div>
</section>