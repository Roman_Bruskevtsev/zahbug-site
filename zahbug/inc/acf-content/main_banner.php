<section class="main__banner">
	<div class="image__block">
		<img src="<?php echo get_theme_file_uri().'/assets/images/banner_image.png'; ?>" alt="<?php _e('image', 'zahbug'); ?>">
	</div>
	<!--<div class="video__block">
		<video autoplay muted>
			<source src="<?php echo get_theme_file_uri().'/assets/video/WEST_BUG.webm'; ?>" type="video/webm">
			<source src="<?php echo get_theme_file_uri().'/assets/video/WEST_BUG.mp4'; ?>" type="video/mp4">
		</video>
	</div> -->
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="text" data-aos="fade-up" data-aos-duration="600">
					<?php if( get_sub_field('title') ) { ?><h5><?php the_sub_field('title'); ?></h5><?php } ?>
					<?php if( get_sub_field('subtitle') ) { ?><h1><?php the_sub_field('subtitle'); ?></h1><?php } ?>
					<?php the_sub_field('text'); ?>
					<?php 
					$link = get_sub_field('link');
					if( $link ) { 
						$target = ($link['target']) ? ' target="'.$link['target'].'"' : ''; ?>
						<a class="btn" href="<?php echo $link['url']; ?>"<?php echo $target; ?>>
							<span class="left"></span>
							<?php echo $link['title']; ?>
							<span class="right"></span>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>