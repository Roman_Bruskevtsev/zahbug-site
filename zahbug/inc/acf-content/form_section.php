<section class="form__section">
	<div class="container">
	<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<h2 class="h3" data-aos="fade-right"><b><?php the_sub_field('title'); ?></b></h2>
			</div>		
		</div>
	<?php } 
	if( get_sub_field('form_shortcode') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="form__block" data-aos="fade-up"><?php echo do_shortcode(get_sub_field('form_shortcode')); ?></div>
			</div>
		</div>
	<?php } ?>
	</div>
</section>