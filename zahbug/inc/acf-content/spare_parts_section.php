<section class="spare__parts__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text" data-aos="fade-right">
				<?php if( get_sub_field('title') ) { ?>
					<h2 class="h3"><b><?php the_sub_field('title'); ?></b></h2>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php 
		$banner = get_sub_field('banner');
		if( $banner ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="banner" data-aos="fade-up">
					<img src="<?php echo $banner['url']; ?>" alt="<?php echo $banner['title']; ?>">
				</div>
			</div>
		</div>
		<?php } 
		$parts = get_sub_field('spare_parts'); 
		if( $parts ) { ?>
		<div class="row">
		<?php foreach ( $parts as $part ) { ?>
			<div class="col-md-6 col-lg-3">
				<div class="spare__block" data-aos="fade-up">
					<h6><?php if( $part['title'] ) { ?><b><?php echo $part['title'].'</b> '.$part['article']; ?><?php } ?></h6>
					<p><?php echo $part['details']; ?></p>
				</div>
			</div>
		<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>