<?php
$blocks = get_sub_field('blocks');
if( $blocks ) { ?>
<section class="images__navigations__block">
	<div class="container">
		<div class="row">
		<?php foreach ( $blocks as $block ) { 
			$background = $block['image'] ? ' style="background-image: url('.$block['image'].')"' : '';
			?>
			<div class="col-md-4">
			<?php if( $block['url'] ) { ?>
				<a class="nav__block" href="<?php echo $block['url']; ?>"<?php echo $background; ?>>
					<?php if( $block['title'] ){ ?><h6><?php echo $block['title']; ?></h6><?php } ?>
				</a>
			<?php } else { ?>
				<div class="nav__block"<?php echo $background; ?>>
					<?php if( $block['title'] ){ ?><h6><?php echo $block['title']; ?></h6><?php } ?>
				</div>
			<?php } ?>
			</div>
		<?php } ?>
		</div>
	</div>
</section>
<?php } ?>