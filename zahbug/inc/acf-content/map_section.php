<section class="map__section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-8">
				<?php 
				$map = get_sub_field('map');
				?>
				<div class="map__block" data-lat="<?php echo $map['map_latitude']; ?>" data-lng="<?php echo $map['map_longitude']; ?>" data-zoom="<?php echo $map['zoom']; ?>" data-marker="<?php echo get_theme_file_uri().'/assets/images/icon_marker.png'; ?>" data-aos="fade-left">
					<ul class="marker__list">
						<li data-lat="<?php echo $map['map_latitude']; ?>" data-lng="<?php echo $map['map_longitude']; ?>"></li>
					</ul>
					<div class="block" id="google__map"></div>
				</div>
			</div>
		</div>
	</div>
	<?php if( get_sub_field('text') ) { ?>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="text" data-aos="fade-right" data-aos-duration="600"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>