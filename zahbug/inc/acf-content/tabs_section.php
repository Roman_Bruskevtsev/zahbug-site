<section class="tabs__section">
	<div class="container">
	<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<h2 class="h3" data-aos="fade-right"><b><?php the_sub_field('title'); ?></b></h2>
			</div>		
		</div>
	<?php } 
	$tabs = get_sub_field('tabs'); 
	if( $tabs ) { ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="tabs">
				<nav class="tabs__nav">
					<ul>
					<?php 
					$i = 0;
					foreach ($tabs as $tab) { 
						$class = $i == 0 ? ' class="active"' : ''; ?>
						<li<?php echo $class; ?> onclick="zahbug.showTab(this);"><?php echo $tab['title'] ?></li>
					<?php $i++; } ?>
					</ul>
				</nav>
				<div class="tabs__block">
				<?php 
				$i = 0;
				foreach ($tabs as $tab) { 
					$class = $i == 0 ? ' active' : ''; ?>
					<div class="tab<?php echo $class; ?>"><?php echo $tab['content']; ?></div>
				<?php $i++; } ?>
				</div>
			</div>
		</div>
	</div>
	<?php  } ?>
	</div>
</section>