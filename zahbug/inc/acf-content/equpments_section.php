<section class="equpments__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text" data-aos="fade-right">
				<?php if( get_sub_field('title') ) { ?>
					<h2 class="h3"><b><?php the_sub_field('title'); ?></b></h2>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php 
		$equpments = get_sub_field('equpments');
		if( $equpments ) { ?>
		<div class="row">
		<?php foreach ( $equpments as $equpment ) { 
			$background = $equpment['image'] ? ' style="background-image: url('.$equpment['image']['url'].');"' : ''; ?>
			<div class="col-lg-6">
				<div class="equpment__block">
					<div class="row">
						<div class="col-lg-10">
							<div class="image"<?php echo $background; ?> data-aos="fade-up"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-10">
							<div class="text" data-aos="fade-left">
								<?php if( $equpment['title'] ) { ?><h6><b><?php echo $equpment['title']; ?></b></h6><?php } ?>
								<?php if( $equpment['text'] ) { ?><p><?php echo $equpment['text']; ?></p><?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>