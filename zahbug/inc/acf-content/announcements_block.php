<section class="announcements__block">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="text">
					<h3><b><?php the_sub_field('title'); ?></b></h3>
				</div>
			</div>
		</div>
		<?php } 
		$blocks = get_sub_field('blocks');
		if( $blocks ) { ?>
		<div class="row">
			<?php foreach ( $blocks as $block ) { ?>
				<div class="col-lg-6">
					<?php if( $block['url'] ) { 
						$target = $block['open_in_new_tab'] ? ' target="_blank"' : ''; ?>
					<a class="announcement__block" href="<?php echo $block['url']; ?>"<?php echo $target; ?>>
						<div class="image__wrapper">
						<?php if( $block['image'] ) { ?>
							<div class="image">
								<img src="<?php echo $block['image']['url']; ?>" alt="<?php echo $block['image']['title']; ?>">
							</div>
						<?php } ?>
						</div>
						<?php if( $block['title'] || $block['text'] ) { ?>
						<div class="content">
							<?php if( $block['title'] ) { ?><h6><?php echo $block['title']; ?></h6><?php } ?>
							<?php if( $block['text'] ) { ?><p><?php echo $block['text']; ?></p><?php } ?>
						</div>
						<?php } ?>
					</a>
					<?php } else { ?>
					<div class="announcement__block">
						<div class="image__wrapper">
						<?php if( $block['image'] ) { ?>
							<div class="image">
								<img src="<?php echo $block['image']['url']; ?>" alt="<?php echo $block['image']['title']; ?>">
							</div>
						<?php } ?>
						</div>
						<?php if( $block['title'] || $block['text'] ) { ?>
						<div class="content">
							<?php if( $block['title'] ) { ?><h6><?php echo $block['title']; ?></h6><?php } ?>
							<?php if( $block['text'] ) { ?><p><?php echo $block['text']; ?></p><?php } ?>
						</div>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>