<?php
/**
 *
 * @package WordPress
 * @subpackage Zahbug
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) { the_post();

		get_template_part( 'template-parts/service/content', 'title' );

		if( have_rows('content') ):
    		while ( have_rows('content') ) : the_row();
        		if( get_row_layout() == 'text_section' ): 
					get_template_part( 'template-parts/service/content', 'text' );
				elseif( get_row_layout() == 'two_column_text_section' ): 
					get_template_part( 'template-parts/service/content', 'two-column-text' );
				elseif( get_row_layout() == 'video_slider' ): 
					get_template_part( 'template-parts/service/content', 'video-slider' );
				elseif( get_row_layout() == 'image_slider' ): 
					get_template_part( 'template-parts/service/content', 'image-slider' );
				elseif( get_row_layout() == 'images_+_text_section' ): 
					get_template_part( 'template-parts/service/content', 'images-text-section' );
				elseif( get_row_layout() == 'services_+_content_section' ): 
					get_template_part( 'template-parts/service/content', 'services-content-section' );
				elseif( get_row_layout() == 'image_+_content_section' ): 
					get_template_part( 'template-parts/service/content', 'image-content-section' );
				elseif( get_row_layout() == 'logos_section' ): 
					get_template_part( 'template-parts/service/content', 'logos-section' );
				elseif( get_row_layout() == 'half_width_blocks_section' ): 
					get_template_part( 'template-parts/service/content', 'half-width-blocks-section' );
				elseif( get_row_layout() == 'products_section' ): 
					get_template_part( 'template-parts/service/content', 'products-section' );
				elseif( get_row_layout() == 'informations_blocks_section' ): 
					get_template_part( 'template-parts/service/content', 'informations-blocks-section' );
				elseif( get_row_layout() == 'text_image_blocks_section' ): 
					get_template_part( 'template-parts/service/content', 'text-image-blocks-section' );
				elseif( get_row_layout() == 'half_text_image_information_block' ): 
					get_template_part( 'template-parts/service/content', 'half-text-image-information-block' );
				elseif( get_row_layout() == 'elevator_services_blocks' ): 
					get_template_part( 'template-parts/service/content', 'elevator-services-blocks' );
				elseif( get_row_layout() == 'map_description_block' ): 
					get_template_part( 'template-parts/service/content', 'map-description-block' );
				endif;
		    endwhile;
		endif;
		$childrens = get_children(
			array(
				'post_parent'	=> get_the_ID(),
				'post_type'		=> 'service'
			)
		);
		
		if( $childrens ){ ?>
		<section class="services__section">
			<div class="container">
				<div class="row">
				<?php foreach ($childrens as $children) { 
					$background = ( get_the_post_thumbnail( $children->ID ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( $children->ID, 'large' ).');"' : '';
					?>
					<div class="col-md-6">
						<a<?php echo $background; ?> href="<?php echo get_the_permalink($children->ID); ?>" class="service__thumbnail large" data-aos="fade-up">
							<h6><span><?php echo $children->post_title; ?></span></h6>
						</a>
					</div>
				<?php } ?>
				</div>
			</div>
		</section>
		<?php }
	}
}

get_footer();