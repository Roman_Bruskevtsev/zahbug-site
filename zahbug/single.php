<?php
/**
 *
 * @package WordPress
 * @subpackage Zahbug
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) { the_post();

		get_template_part( 'template-parts/post/content', 'title' );

		if( have_rows('content') ):
    		while ( have_rows('content') ) : the_row();
        		if( get_row_layout() == 'text_section' ): 
					get_template_part( 'template-parts/post/content', 'text' );
				elseif( get_row_layout() == 'two_column_text_section' ): 
					get_template_part( 'template-parts/post/content', 'two-column-text' );
				elseif( get_row_layout() == 'video_slider' ): 
					get_template_part( 'template-parts/post/content', 'video-slider' );
				elseif( get_row_layout() == 'image_slider' ): 
					get_template_part( 'template-parts/post/content', 'image-slider' );
				endif;
		    endwhile;
		endif;

		get_template_part( 'template-parts/post/content', 'sharing' );
	}
}

get_footer();